
import React, {Component} from 'react';
import { NetInfo, ActivityIndicator, SafeAreaView, AsyncStorage, Alert, Image, ScrollView, TextInput, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase';
var ImagePicker = require('react-native-image-picker');
import Icon_II from 'react-native-vector-icons/Ionicons';
import { normalize } from './Utils';
import ImageResizer from 'react-native-image-resizer';

// More info on all the options is below in the README...just some common use cases shown here
var options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class AddItem extends Component {

    static navigationOptions = ({ navigation }) => ({
        header: null
    });
    constructor(){
        super()
        this.getImage = this.getImage.bind(this);
        this.deleteDrinkItem = this.deleteDrinkItem.bind(this);
        this.state = {
          image_uri: '',
          types: [],
          typeName: '',
          typeIndex: 0,
          brandId: '',
          brandName: '',
          companyName: '',
          packageOneName: '',
          packageOnePrice: '',
          packageTwoName: '',
          packageTwoPrice: '',
          packageThreeName: '',
          packageThreePrice: '',  
          brandObject: '',   
          locationObj: {}, 
          isLoading: false,
          localImage: '',
          downloadURL: 'https://firebasestorage.googleapis.com/v0/b/bottomsapp-c9207.appspot.com/o/drinks%2Fplaceholder228.png?alt=media&token=8948e3c8-db19-4bdf-bc8d-4988ca6a463b',
        }
    }
    addCategory = (navigate) => {
        console.log(`typeIndex=${JSON.stringify(this.state.locationObj)} types=${this.state.types}`);
        NetInfo.isConnected.fetch().then(isConnected => {
            console.log('Internet is ' + (isConnected ? 'online' : 'offline'));
            if(isConnected) {
                navigate("AddCategoryScreen", 
                {
                updateFromCategory:this.updateFromCategory, 
                locationObj:this.state.locationObj,
                types: this.state.types 
                });
            } else {
              alert('Please connect to the internet');
            }
        }); 
        
    }
    updateFromCategory  = (dataTypes) => {
        console.log('updating user data coming from : '+dataTypes);
        if(dataTypes) {
            this.setState({ types: dataTypes});
        } else {
            this.fetchDataTypes();
        }
    };
    handleBrandname = (text) => {
        this.setState({ brandName: text });
    }
    handleCompanyName = (text) => {
        this.setState({ companyName: text });
    }
    handlePackageOneName = (text) => {
        this.setState({ packageOneName: text });
    }
    handlePackageOnePrice = (text) => {
        this.setState({ packageOnePrice: text });
    }
    handlePackageTwoName = (text) => {
        this.setState({ packageTwoName: text });
    }
    handlePackageTwoPrice = (text) => {
        this.setState({ packageTwoPrice: text });
    }
    handlePackageThreeName = (text) => {
        this.setState({ packageThreeName: text });
    }
    handlePackageThreePrice = (text) => {
        this.setState({ packageThreePrice: text });
    }
    _pressRow = (index) => {
        this.setState({ typeIndex: index, brandId: '', index: -1});        
    }
    async componentWillMount() {
        console.log(`addItem1 out deepak`);
        var typeIndex = this.props.navigation.getParam('typeIndex', '')>0 ? 
        this.props.navigation.getParam('typeIndex', '') : 0 ;
        console.log(`addItem2 out deepak ${JSON.stringify(typeIndex)}`);
        var brandObj = this.props.navigation.getParam('brandObject', '');
        console.log(`addItem2 out deepak ${JSON.stringify(brandObj)}`);
        if(!this.state.brandObject) {
            this.setState({
                brandObject: this.props.navigation.getParam('brandObject', ''),
                types: this.props.navigation.getParam('types', ''),
                typeIndex: this.props.navigation.getParam('typeIndex', '')>0 ? this.props.navigation.getParam('typeIndex', '') : 0,
                brandName: brandObj ? brandObj.brandName : '',
                brandId: brandObj ? brandObj.brandId : '',
                companyName: brandObj.company,
                packageOneName: brandObj.packageSize ? brandObj.packageSize[0] ? brandObj.packageSize[0].name : '' : '',
                packageOnePrice: brandObj.packageSize ? brandObj.packageSize[0] ? brandObj.packageSize[0].price : '' : '',
                packageTwoName: brandObj.packageSize ? brandObj.packageSize[1] ? brandObj.packageSize[1].name : '' : '',
                packageTwoPrice: brandObj.packageSize ? brandObj.packageSize[1] ? brandObj.packageSize[1].price : '' : '',
                packageThreeName: brandObj.packageSize ? brandObj.packageSize[2] ? brandObj.packageSize[2].name : '' : '',
                packageThreePrice: brandObj.packageSize ? brandObj.packageSize[2] ? brandObj.packageSize[2].price : '' : '',
                image_uri: brandObj ? brandObj.brandImage : '',
                locationObj: this.props.navigation.getParam('locationObj', ''),
                index: this.props.navigation.getParam('index', -1),
                fullData: this.props.navigation.getParam('fullData', ''),
            });
        }
        else {
            this.setState({ locationObj: this.props.navigation.getParam('locationObj', ''),
            types: this.props.navigation.getParam('types', ''),
            typeIndex: this.props.navigation.getParam('typeIndex', '')>0 ? this.props.navigation.getParam('typeIndex', '') : 0,
            index: this.props.navigation.getParam('index', -1),
            fullData: this.props.navigation.getParam('fullData', ''),
            });
        }
        console.log(`location AddItemScreen:${JSON.stringify(this.state.locationObj)} typeIndex=${this.state.typeIndex}
        types= ${this.state.types}`);
           
    }
    async componentDidMount() {
        if(!this.state.types||this.state.types.length==0) {
            this.fetchDataTypes();
        }
    }
    fetchDataTypes = async() => {
        try {
            var dataLocation = this.state.locationObj.id;
            console.log(`location fetchingDataTypes:${dataLocation}`);
            var recentPostsRef = firebase.database().ref('bottomsapp/types/'+dataLocation);
            console.log(`recentPostsRef1 : ${recentPostsRef}`);
            recentPostsRef.once('value').then(snapshot => {
                console.log(`types : ${JSON.stringify(snapshot.toJSON())}`);
                if(snapshot&&snapshot.toJSON()) {
                    var localTypes = snapshot.toJSON();
                    var typesArray = new Array(Object.keys(localTypes).length+1);
                    Object.keys(localTypes).map(function(key) {
                        console.log('key='+key+' data='+JSON.stringify(localTypes[key]));
                        typesArray[localTypes[key].index] = {'id': key, 'type': localTypes[key].type};
                    });
                    this.setState({types: typesArray});
                    console.log(`types : ${JSON.stringify(this.state.types[0].id)}`);
                }                
            });
        } catch(error) {
            console.log(error);
        }
    }
    sendItem = (typeId, brandId, brandName, companyName, packageOneName, packageOnePrice, 
        packageTwoName, packageTwoPrice, packageThreeName, packageThreePrice) => {
        console.log('typeId: ' + typeId + ' brandId= ' + brandId + ' brandName: ' + brandName + 'company: ' + companyName 
        + ' bran1name: ' + packageOneName + ' bran1price: ' + packageOnePrice + ' bran2name: ' + packageTwoName
        + ' bran2price: ' + packageTwoPrice + ' bran3name: ' + packageThreeName + 
        ' bran3price: ' + packageThreePrice + ' image: '+ this.state.image_uri);
                
        console.log('url='+this.state.image_uri);
        if(this.state.image_uri.includes('http')) {
            console.log('img include http');
        }    

        if(brandName==''||packageOneName==''||packageOnePrice=='') {
            Alert.alert('Please fill all the required fields.');
        }
        else {
            try {
                console.log(`typeIndex ${this.state.typeIndex} : ${this.state.types.length}`);
                var packageSize = [];
                if(packageOneName!=''&&packageOnePrice!='') {
                    packageSize.push({name:packageOneName.trim(), price:packageOnePrice.trim()});
                }
                if(packageTwoName!=''&&packageTwoPrice!='') {
                    packageSize.push({name:packageTwoName.trim(), price:packageTwoPrice.trim()});
                }
                if(packageThreeName!=''&&packageThreePrice!='') {
                    packageSize.push({name:packageThreeName.trim(), price:packageThreePrice.trim()});
                }
                var index = this.state.index;
                console.log('idex='+index+' fulldata='+JSON.stringify(this.state.fullData));
                if(index<0) {
                    if(this.state.fullData[typeId]) {
                        index = Object.keys(this.state.fullData[typeId]).length;
                    }
                    else {
                        index = 0;
                    }
                }
                console.log('idex='+index);
                let randomNum = Math.floor(Math.random()*90000) + 10000;
                brandId = brandId ? brandId : new Date().getTime()+''+randomNum;
                var drinkObj = { brandId: brandId, 
                    brandName: brandName.trim(), 
                    brandImage: this.state.image_uri ? this.state.image_uri : this.state.downloadURL, 
                    company: companyName?companyName.trim():'', packageSize: packageSize,
                    selectedSize: 0, index: index,
                };
                console.log('drinkObj='+JSON.stringify(drinkObj));
                firebase.database().ref('bottomsapp/subtypes/'+typeId).update({
                    [brandId] : drinkObj
                }, (error) => {
                    if (error) {
                        console.log(`got an error=${error}`);
                    } else {
                        console.log(`data saved on firebase`);
                        this.handleBackPress();
                    }
                  });
            } catch(error) {
                  console.log(error);
            }
        }
    }
    handleBackPress() {
        console.log(`handing back press Add drink. ${JSON.stringify(this.props.navigation.state)}`);
        this.props.navigation.goBack();
        this.props.navigation.state.params.updateDataAddItem(this.state.typeIndex);
    }
    deleteDrink = () => {
        console.log('OK Pressed');
        try {
            var typeId = this.state.types[this.state.typeIndex].id;
            var brandId = this.state.brandId;
            console.log('typeId='+typeId+' brandId='+brandId);
            firebase.database().ref('bottomsapp/subtypes/' + typeId+'/'+brandId).remove(
                (error) => {
                    if (error) {
                        console.log(`got an error=${error}`);
                    } else {
                        console.log(`data saved on firebase`);
                        this.handleBackPress();
                    }
                  }
            );            
        } catch(error) {
              console.log(error);
        }
    }
    deleteDrinkItem = () => {
        console.log('delete drink action');
        if(this.state.brandId) {
            Alert.alert(
                'Deleting Drink',
                'Do you really want to delete this drink?',
                [
                  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                  {text: 'OK', onPress: () => this.deleteDrink()},
                ],
                { cancelable: false }
            );
        }
        else {
            Alert.alert(
                'There is nothing to delete.'
            );
        }
    }
    getImage = () => {
        console.log('getting image time='+new Date().getTime());
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response9 = is here'+new Date().getTime());
            this.setState({isLoading:true});
            if (response.didCancel) {
              this.setState({isLoading:false});
              console.log('User cancelled photo picker');
            }
            else if (response.error) {
                this.setState({isLoading:false});
                console.log('ImagePicker Error: '+response.error);
            }
            else if (response.customButton) {
                this.setState({isLoading:false});
                console.log('User tapped custom button: '+response.customButton);
            }
            else {
                console.log('ImagePicker uri: '+response.uri+' time taken='+new Date().getTime());
                
                ImageResizer.createResizedImage(response.uri, 228, 228, 'JPEG', 100).then((resizedImageUri) => {
                    console.log('ImagePicker resize time: '+new Date().getTime());
                    
                    firebase.storage().ref().child(`drinks/drink${new Date().getTime()}.jpg`)
                    .put(resizedImageUri.uri, { contentType : 'image/jpeg' })
                    .then((snapshot) => {
                        console.log('download url='+JSON.stringify(snapshot.downloadURL)+' time='+new Date().getTime());
                        this.setState({ image_uri: snapshot.downloadURL, isLoading: false, localImage: resizedImageUri.uri });
                    }).catch(err => {
                        this.setState({isLoading:false});
                        console.log("error : " +err);
                    });

                  }).catch((err) => {
                    this.setState({isLoading:false});
                    console.log(err);
                  });

              
            }
        });    
    }
    
    
    render() {
        const { navigate } = this.props.navigation;
        console.log(`types=${this.state.localImage} brandObject=${JSON.stringify(this.state.brandObject)}`);
        return (
            <SafeAreaView style={styles.safeArea}>
            <View style={[styles.container, ]}>
            <View style={styles.topBar}>
                <View style={styles.topBarContainer}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.handleBackPress()} style={styles.leftContainer}>
                    <Icon_II name="md-arrow-round-back" style={styles.icons}/>
                    <View style={styles.location}>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation1}>
                        BottomsApp
                        </Text>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation2}>
                        {this.state.locationObj.name}
                        </Text>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.rightContainer}
                    onPress = {
                        () => this.sendItem(this.state.types[this.state.typeIndex].id, this.state.brandId, this.state.brandName, this.state.companyName, 
                        this.state.packageOneName, this.state.packageOnePrice, this.state.packageTwoName, 
                        this.state.packageTwoPrice, this.state.packageThreeName, this.state.packageThreePrice)
                    }>
                    <Icon_II name="md-cloud-upload" style={styles.iconUpload}/>
                    </TouchableOpacity>
                </View>
            </View>  
            <View style={styles.category}>
              <View style = {styles.scrollViewType}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {this.state.types&&this.state.types.length>0&&
                  this.state.types.map((item, index) => (
                    <View key = {item.id} style = {{ flexDirection: 'row', alignItems: 'center',}}>
                    <TouchableOpacity onPress={() => this._pressRow(index)}>
                    <Text allowFontScaling={false} numberOfLines={1} 
                    style={[styles.categoryText, { color: index==this.state.typeIndex ? '#14cda8' : '#7B8B9B' }]}>
                    {item&&item.type ? item.type : 'type1'}
                    </Text>
                    </TouchableOpacity>  
                    </View>
                  ))
                }
              </ScrollView>
              </View>
              <View style = {styles.addType} >
              <TouchableOpacity 
               onPress = {() => this.addCategory(navigate)}>
                <Icon_II name="md-add-circle" style={{fontSize:normalize(36), color:'#aaa'}}/>
              </TouchableOpacity>
              </View>
            </View>
            <ScrollView  style={styles.content}
            keyboardDismissMode='on-drag'>
            <TextInput allowFontScaling={false} style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Brand Name"
               value={this.state.brandName}
               placeholderTextColor = "#CDCDCD"
               autoCapitalize = "none"
               onChangeText = {this.handleBrandname}/>

            <TextInput allowFontScaling={false} style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Company (Optional)"
               value={this.state.companyName}
               placeholderTextColor = "#CDCDCD"
               autoCapitalize = "none"
               onChangeText = {this.handleCompanyName}/>

            <View style={styles.packageBody}>
                <View style={styles.packageItem}>
                    <TextInput allowFontScaling={false} style = {styles.packageItemName}
                        underlineColorAndroid = "transparent"
                        placeholder = "Package Name"
                        value={this.state.packageOneName}
                        placeholderTextColor = "#CDCDCD"
                        onChangeText = {this.handlePackageOneName}/>
                    <TextInput allowFontScaling={false} style = {styles.packageItemPrice}
                        underlineColorAndroid = "transparent"
                        placeholder = "Price"
                        value={`${this.state.packageOnePrice}`}
                        keyboardType='numeric'
                        returnKeyType='go'
                        placeholderTextColor = "#CDCDCD"
                        onChangeText = {this.handlePackageOnePrice}/>    
                </View>
                <View style={{height:8}}></View>
                <View style={styles.packageItem}>
                    <TextInput allowFontScaling={false} style = {styles.packageItemName}
                        underlineColorAndroid = "transparent"
                        placeholder = "Package Name"
                        value={this.state.packageTwoName}
                        placeholderTextColor = "#CDCDCD"
                        onChangeText = {this.handlePackageTwoName}/>
                    <TextInput allowFontScaling={false} style = {styles.packageItemPrice}
                        underlineColorAndroid = "transparent"
                        placeholder = "Price"
                        value={`${this.state.packageTwoPrice}`}
                        keyboardType = 'numeric'
                        returnKeyType='go'
                        placeholderTextColor = "#CDCDCD"
                        onChangeText = {this.handlePackageTwoPrice}/> 
                </View>
                <View style={{height:8}}></View>
                <View style={styles.packageItem}>
                    <TextInput allowFontScaling={false} style = {styles.packageItemName}
                        underlineColorAndroid = "transparent"
                        placeholder = "Package Name"
                        value={this.state.packageThreeName}
                        placeholderTextColor = "#CDCDCD"
                        onChangeText = {this.handlePackageThreeName}/>
                    <TextInput allowFontScaling={false} style = {styles.packageItemPrice}
                        underlineColorAndroid = "transparent"
                        placeholder = "Price"
                        value={`${this.state.packageThreePrice}`}
                        keyboardType = 'numeric'
                        returnKeyType='go'
                        placeholderTextColor = "#CDCDCD"
                        onChangeText = {this.handlePackageThreePrice}/> 
                </View>
            </View>
            {this.state.isLoading&&
            <View style={styles.loading}>
                <ActivityIndicator size='large' color="#14cda8" />
            </View>}
            <View style={{flex:1, height:166, flexDirection: 'row', margin:10,}}>
                <TouchableOpacity onPress = {() => this.getImage()}
                style={[styles.imageUpload,{ alignItems: this.state.image_uri?'stretch':'center'}]}>
                    {this.state.image_uri
                    ? <Image style={styles.drinkImage} source={{isStatic:true, uri: 
                    this.state.localImage ? this.state.localImage : this.state.image_uri}} />
                    :  <Icon_II name="md-image" style={{ fontSize:normalize(64), color:'#aaa'}}/>}
                </TouchableOpacity>
                <View style={{flex:6, flexDirection: 'column', justifyContent:'space-evenly', marginLeft:10, }}>
                <TouchableOpacity
                    style = {styles.deleteDrinkStyle}
                    onPress = {() => this.deleteDrinkItem()}>
                    <Icon_II name="md-trash" style={{fontSize:normalize(48), color:'#aaa'}}/>
                </TouchableOpacity>
                </View>
            </View>
            
            <View style={{flex:1, height:100, }}></View>
            </ScrollView> 
             
          </View>
          </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#ececec'
    },
    topBar: {
        height: 64,
    },
    topBarContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    leftContainer: {
        flex: 6,
        flexDirection: 'row',
        backgroundColor: '#23395B',
    },
    rightContainer: {
        flex: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#14cda8',
    },
    icons: {
        fontSize: normalize(32),
        color: '#CDCDCD',
        alignSelf: 'center',
        marginLeft: 12,
    },
    location: {
        flex: 1,
        height: 64,
        flexDirection: 'column',
        paddingLeft: 16,
        alignItems: 'flex-start',
        justifyContent: 'center',
        
    },
    textLocation1: {
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(22),
        color: '#CDCDCD',
        textAlign: 'center',
        flexWrap: 'wrap',
    },
    textLocation2: {
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(16),
        color: '#CDCDCD',
        textAlign: 'center',
    },
    iconUpload: {
        fontSize: normalize(36),
        color: '#23395b',
    },
    footer: {
        height: 64,
        justifyContent: 'center',
        textAlign: 'center', 
        alignItems: 'center',
        backgroundColor: '#37495A',
    },  
    content: {
        flex: 1,
    },
    category: {
        flexDirection: 'row',
        height: 64,
    },
    categoryText: {
        marginLeft:16,
        marginRight:16,
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(20),
        textAlign: 'center',
    },
    addType: {
        width: 64,
        justifyContent: 'center',
        alignItems: 'center',
    },
    scrollViewType: {
        flex: 28,
    },
    input: {
        marginLeft: 10,
        marginRight: 10,
        paddingLeft: 10,
        marginBottom: 12,
        height: 48,
        fontFamily: "RobotoSlab-Light",
        fontSize: normalize(24),
        borderColor: '#aaaaaa',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderBottomColor: '#aaaaaa',
     },
     dialogButton: {
        backgroundColor: '#2980b6',
        padding: 10,
        margin: 15,
        height: 40,
     },
     deleteDrinkStyle: {
        flex:3, 
        flexDirection: 'row',
        borderColor:'#aaa', 
        borderWidth: 1, 
        backgroundColor:'#fff', 
        justifyContent: 'center',
        alignItems: 'center',
     },
     imageUpload: {
        flex:4, 
        borderColor:'#aaa', 
        borderWidth: 1, 
        backgroundColor:'#fff', 
        justifyContent:'center',
     },
    packageBody: {
        flex: 1,
        height: 156,
        margin: 10,
        flexDirection: 'column', 
    },  
    packageItem: {
        flex: 1,
        flexDirection: 'row',  
        justifyContent: 'center',
        alignItems: 'center',
    },
    packageItemName: {
        flex: 8,
        height: 48,
        fontFamily: "RobotoSlab-Light",
        fontSize: normalize(24),
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        borderColor: '#aaaaaa',
        borderWidth: 1,
    },
    packageItemPrice: {
        flex: 2,
        height: 48,
        fontFamily: "RobotoSlab-Light",
        backgroundColor: '#fff',
        fontSize: normalize(24),
        marginLeft: 8,
        paddingLeft: 10,
        paddingRight: 10,
        borderColor: '#aaaaaa',
        borderWidth: 1,
    },
    drinkImage: {
        flex: 1,
        width: null,
        height: null,
    },
});