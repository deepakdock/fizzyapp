import React, {Component} from 'react';
import { FlatList, ActivityIndicator, SafeAreaView, Switch, ScrollView, TextInput, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase';
import Icon_II from 'react-native-vector-icons/Ionicons';
import { normalize } from './Utils';

export default class AddLocation extends Component {
   
    static navigationOptions = ({ navigation }) => ({
        header: null
    });
    constructor(){
        super()
        console.log(`addLocation constructor called`);
        this.state = {
          locationsArray: [],
          locationObj: {},
        }
    }
    componentWillMount() {
        console.log(`addCat1 out deepak ${this.props.navigation}`);
        var locationsArray = this.props.navigation.getParam('locationsArray', '');
        var locationObj = this.props.navigation.getParam('locationObj');
        console.log(`addCat2 AddLocationScreen:${(JSON.stringify(locationObj))}`);
        this.initialSetup(locationsArray,locationObj);
    }
    initialSetup(locationsArray, locationObj) {
        var tempLocations = Object.assign([], locationsArray);
        let length = locationsArray.length;
        let randomNum = Math.floor(Math.random()*90000) + 10000;
        let id =new Date().getTime()+''+randomNum;
        let tempItem = {"id":id,"name":'', "index":length, "hide":false};
        tempLocations.push(tempItem);
        console.log(`add location tempItem= ${JSON.stringify(tempLocations)}`);
        this.setState({
            locationsArray: tempLocations,
            locationObj: locationObj,
        });
        console.log(`addlocaton locationArr= ${JSON.stringify(this.state.locationsArray)}`);
    }
    handleBackPress(locations) {
        console.log(`handing back3 press addlocation ${JSON.stringify(locations)}`);
        let tempLocations = [];
        for(var i=0;i<locations.length;i++) {
            if(locations[i].name!='') {
                tempLocations.push(locations[i]);
            }
        }
        this.props.navigation.goBack();
        this.props.navigation.state.params.updateFromAddLocation(tempLocations);
    }
    handleLocationChange = (text, item, index) => {
        let tempLocations = [ ...this.state.locationsArray ];
        
            let tempItem = item;
            tempItem.name = text.trim();
            tempLocations[index] = tempItem;
            console.log(`tempItem= ${tempItem}`);
        
        console.log(`handle updated locations ${JSON.stringify(tempLocations)}`);
        this.setState({
            locationsArray: tempLocations
        });
    }
    sendItemPressed = () => {
        console.log(`sendItem pressed locations= ${JSON.stringify(this.state.locationsArray)}`);
        let uploadLocations = {};
        let oldLocations = [...this.state.locationsArray];
        for(var i=0;i<oldLocations.length;i++) {
            if(oldLocations[i].name!='') {
                uploadLocations[oldLocations[i].id] = {'index': i, 'name': oldLocations[i].name,
                'hide': oldLocations[i].hide };
            }
        }
        console.log(`sendItem2 pressed locations= ${JSON.stringify(uploadLocations)}`);
        try {
            firebase.database().ref('bottomsapp/').update({
                locations : uploadLocations
            }, (error) => {
                if (error) {
                    console.log(`got an error=${error}`);
                } else {
                    console.log(`data saved on firebase`);
                    this.handleBackPress(uploadLocations);
                }
            });
        } catch(error) {
            console.log(error);
        }
    }
    toggleSwitch = (index, value) => {
        console.log('toggle index='+index+' value='+value);
        let tempLocations = [...this.state.locationsArray];
        var tempLocation = tempLocations[index];
        tempLocation.hide = !value;
        tempLocations[index] = tempLocation;
        this.setState({locations: tempLocations});
        console.log('locations updated='+JSON.stringify(tempLocations));
    }
    _keyExtractor = (item, index) => `${item.id}${item.sizeIndex}`;
    renderItem(data) {
        let { item, index } = data;
        console.log(`item=${JSON.stringify(item)}`);
        return (    
          <View style={styles.cardItem}>
            <TextInput allowFontScaling={false} style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Location"
               value={this.state.locationsArray[index].name}
               placeholderTextColor = "#CDCDCD"
               autoCapitalize = "none"
               onChangeText = {(text) => this.handleLocationChange(text, item, index)}/>   
                
                <Switch
                    onValueChange = {(value) => this.toggleSwitch(index, value) }
                    value = {!item.hide}/>
          </View>    
        );
      }
    render() {
        console.log(`rendering add location`);
        return (
            <SafeAreaView style={styles.safeArea}>
            <View style={[styles.container, ]}>
            <View style={styles.content}>
            <View style={styles.topBar}>
                <View style={styles.topBarContainer}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.handleBackPress(this.state.locationsArray)} style={styles.leftContainer}>
                    <Icon_II name="md-arrow-round-back" style={styles.icons}/>
                    <View style={styles.location}>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation1}>
                        Add Location
                        </Text>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation2}>
                        {this.state.locationObj.name}
                        </Text>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.rightContainer}
                    onPress = {
                        () => this.sendItemPressed()
                    }>
                    <Icon_II name="md-cloud-upload" style={styles.iconUpload}/>
                    </TouchableOpacity>
                </View>
            </View>
            <ScrollView  style={styles.content}
            keyboardDismissMode='on-drag'>
            
            <FlatList
                data={this.state.locationsArray}
                extraData={this.state}
                keyExtractor={this._keyExtractor}
                renderItem={this.renderItem.bind(this)}
            />
            <View style={{flex:1, height:300, }}></View>
            </ScrollView>
            
            </View>  
          </View>
          </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    content: {
        flex: 1,
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#ececec'
    },
    topBar: {
        height: 64,
    },
    arrowButton: {
        justifyContent:'center', 
        padding:12, 
        height: 48,
    },
    locationsView: {
        flexDirection: 'row',
        height: 64,
    },
    locationText: {
        marginLeft:16,
        marginRight:16,
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(20),
        textAlign: 'center',
    },
    location: {
        flex: 1,
        height: 64,
        flexDirection: 'column',
        paddingLeft: 16,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    topBarContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    leftContainer: {
        flex: 6,
        flexDirection: 'row',
        backgroundColor: '#23395B',
    },
    rightContainer: {
        flex: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#14cda8',
    },
    icons: {
        fontSize: normalize(32),
        color: '#CDCDCD',
        alignSelf: 'center',
        marginLeft: 12,
    },
    iconUpload: {
        fontSize: normalize(36),
        color: '#23395b',
    },
    drinkImage: {
        flex: 1,
        width: null,
        height: null,
    },
    textLocation1: {
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(22),
        color: '#CDCDCD',
        textAlign: 'center',
        flexWrap: 'wrap',
    },
    textLocation2: {
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(16),
        color: '#CDCDCD',
        textAlign: 'center',
    },
    input: {
        flex:1,
        alignItems: 'center',
        fontFamily: "RobotoSlab-Light",
        fontSize: normalize(24),
     },
     cardItem: {
        flex: 1,
        height: 48,
        flexDirection: 'row',
        paddingLeft:12, 
        paddingRight:12, 
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:12,
        marginLeft:12,
        marginRight:12,
        borderColor:'#aaaaaa',
        borderWidth:1,
        backgroundColor:'#fff'
      },
});