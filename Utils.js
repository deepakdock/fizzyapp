import { Dimensions, Platform, PixelRatio } from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale1 = size => SCREEN_WIDTH / guidelineBaseWidth * size;
const verticalScale = size => SCREEN_HEIGHT / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale1(size) - size ) * factor;

const baseWidth = 375;
const baseHeight = 667;

const scaleWidth = SCREEN_WIDTH / baseWidth;
const scaleHeight = SCREEN_HEIGHT / baseHeight;
const scaleOF = Math.min(scaleWidth, scaleHeight);


//Redmi3S     ratioX=0.875 width=360 pixelRatioFont=1.149999976158142 pixelRatio=2
//Mi4i        ratioX=0.875 width=360 pixelRatioFont=1 pixelRatio=3
// Precalculate Device Dimensions for better performance
const x = Dimensions.get('window').width;
const y = Dimensions.get('window').height;

// Calculating ratio from iPhone breakpoints
const ratioX = x < 375 ? (x < 320 ? 0.75 : 0.875) : 1 ;
const ratioY = y < 568 ? (y < 480 ? 0.75 : 0.875) : 1 ;

// We set our base font size value
const base_unit = 16;

// We're simulating EM by changing font size according to Ratio
const unit = base_unit * ratioX;

randomNum = () => {
  var num = Math.floor(Math.random()*90000) + 10000;
  return num;
}
export function normalize(size) {
  //console.log('size='+size+' ratioX='+ratioX+' width='+x+' pixelRatioFont='+PixelRatio.getFontScale()+' pixelRatio='+PixelRatio.get());
  
  if (Platform.OS === 'ios') {
    return size*ratioX;
  } else {
    return size/PixelRatio.getFontScale();
  }
  /*
  const newSize = size * scale;
  //console.log('font size='+size+' newsize='+newSize+' width='+SCREEN_WIDTH+" height="+SCREEN_HEIGHT);
  //console.log('font scale1='+scale1(size)+' scale12='+scale1(newSize));
  //console.log('font verticalScale1='+verticalScale(size)+' verticalScale2='+verticalScale(newSize));
  //console.log('font moderateScale1='+moderateScale(size)+' moderateScale2='+moderateScale(newSize));
  return Math.ceil((size * scaleOF));
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
  */
}
