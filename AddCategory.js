import React, {Component} from 'react';
import { FlatList, AsyncStorage, ActivityIndicator, SafeAreaView, Platform, Alert, Image, ScrollView, TextInput, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase';
import Icon_II from 'react-native-vector-icons/Ionicons';
import { normalize } from './Utils';

export default class AddCategory extends Component {
   
    static navigationOptions = ({ navigation }) => ({
        header: null
    });
    constructor(){
        super()
        console.log(`addCat1 constructor called`);
        this.state = {
          types: [],
          typeName: '',
          locationObj: {},
          locationsCategory: [],
          typeIndex: 0,
          isFirstTime: true,
          isLoading: true,
        }
    }
    componentWillMount() {
        console.log(`addCat1 out deepak ${this.props.navigation}`);
        var location = this.props.navigation.getParam('locationObj');
        this.setState({ locationObj: location});
        console.log(`addCat2 location ${location} AddCategoryScreen}`);
    }
    async componentDidMount() {
        this.fetchLocations();
    }
    fetchLocations = async() => {
        try {
            var location = this.state.locationObj.name;
            var tempIndex=this.state.typeIndex;
            var recentPostsRef = firebase.database().ref('bottomsapp/locations');
            console.log(`fetching location data url : ${recentPostsRef} location=${location}`);
            recentPostsRef.once('value').then(snapshot => {
                if(snapshot) {
                    var tempLocations = snapshot.toJSON();
                    var locationArray = new Array(Object.keys(tempLocations).length);
                    var i=0;
                    var isFirst = this.state.isFirstTime;
                    Object.keys(tempLocations).map(function(key) {
                        console.log('key='+key+' data='+JSON.stringify(tempLocations[key]));
                        locationArray[tempLocations[key].index] = {'id':key, 'name':tempLocations[key].name,
                        'hide':tempLocations[key].hide };
                        if(isFirst&&tempLocations[key].name==location) {
                            tempIndex=tempLocations[key].index;  
                            isFirst=false;
                        }
                        i++;
                    });
                    console.log(`result locations= ${JSON.stringify(locationArray)}`);
                    this.fetchTypesData(locationArray[tempIndex], tempIndex);
                    this.setState({ locationsCategory: locationArray, typeIndex: tempIndex, 
                        isFirstTime:false, isLoading:false});
                }             
            });
        } catch(error) {
            console.log(error);
        }
    }
    _pressRow = (index) => {
        var locationObj = this.state.locationsCategory&&this.state.locationsCategory[index] 
        ? this.state.locationsCategory[index] : {};        
        if(locationObj) {
            (async () => {
                await this.fetchTypesData(locationObj, index);  
            })();  
        }          
    }
    fetchTypesData = async(locationObj, index) => {
        try {
            locationObj = locationObj ? locationObj : this.state.locationObj;
            console.log(`location fetchingDataTypes:${JSON.stringify(locationObj)}`);
            var recentPostsRef = firebase.database().ref('bottomsapp/types/'+locationObj.id);
            console.log(`recentPostsRef1 : ${recentPostsRef}`);
            recentPostsRef.once('value').then(snapshot => {
                if(snapshot) {
                    var typesObj = snapshot.toJSON() ? snapshot.toJSON() : {};
                    //console.log(`types : ${JSON.stringify(typesObj)}`);
                    let randomNum = Math.floor(Math.random()*90000) + 10000;
                    let id = new Date().getTime()+''+randomNum;
                    console.log('new id='+id);
                    let length = Object.keys(typesObj).length;
                    var tempTypes = new Array(length+1);
                    let tempItem = {"id": id, "type":'', "index":length};
                    tempTypes[length] = tempItem;
                    Object.keys(typesObj).map(function(key) {
                        //console.log('key='+key+' data='+JSON.stringify(typesObj[key]));
                        tempTypes[typesObj[key].index] = {'id': key, 
                        'type': typesObj[key].type, 'index':typesObj[key].index};
                    });
                    console.log(`add category tempItem= ${JSON.stringify(tempTypes)}`);
                    this.setState({ types: tempTypes, typeIndex: index });
                    console.log(`types : ${JSON.stringify(this.state.types)}`); 
                }               
            });
        } catch(error) {
            console.log(error);
        }
    }
    handleBackPress() {
        console.log(`handing back2 press addCategory ${JSON.stringify(this.props.navigation.state)}`);
        this.props.navigation.goBack();
        this.props.navigation.state.params.updateFromCategory();
    }
    addLocation = (navigate) => {
        console.log(`typeIndex=${this.state.typeIndex} locationsCategory=${JSON.stringify(this.state.locationsCategory)}`);
        navigate("AddLocationScreen", 
            {
            updateFromAddLocation:this.updateFromAddLocation, 
            locationsArray:this.state.locationsCategory,
            locationObj:this.state.locationObj,
            });
    }
    updateFromAddLocation  = (data) => {
        console.log('updating user data coming from AddLocation : '+JSON.stringify(data)
        +' typeIndex='+this.state.typeIndex);
        this.fetchLocations();
        if(data&&data.length>0) {
            //this.setState({ locationsCategory: data}); 
        }
        console.log('locationsCategory : '+JSON.stringify(this.state.locationsCategory));
    };
    handleCategoryChange = (text, item, index) => {
        let tempTypes = [ ...this.state.types ];
        
            let tempItem = item;
            tempItem.type = text.trim();
            tempTypes[index] = tempItem;
            console.log(`tempItem= ${tempItem}`);
        
        console.log(`handle updated types ${JSON.stringify(tempTypes)}`);
        this.setState({
            types: tempTypes
        });
    }
    moveItemUp = (index) => {
        if(index>0) {
            let tempData = [...this.state.types];
            this.swapItems(tempData, index, index - 1);
            this.setState({types: tempData});
            console.log(` updated types ${JSON.stringify(this.state.types)}`);
        }
    }
    moveItemDown = (index) => {
        if (index < this.state.types.length - 1) {
            let tempData = [...this.state.types];
            this.swapItems(tempData, index, index + 1);
            this.setState({types: tempData});
            console.log(` updated types ${JSON.stringify(this.state.types)}`);
        }
    }
    swapItems = (obj, prop1, prop2) => {
        var tmp = obj[prop1];
        obj[prop1] = obj[prop2];
        obj[prop2] = tmp;
    }
    sendItemPressed = () => {
        var dataLocation = this.state.locationsCategory[this.state.typeIndex].id;
        console.log(`sendItem pressed location=${dataLocation} types= ${JSON.stringify(this.state.types)}`);
        let uploadTypes = {};
        let oldTypes = [...this.state.types];
        for(var i=0;i<oldTypes.length;i++) {
            if(oldTypes[i].type!='') {
                uploadTypes[oldTypes[i].id] = {'index': i, 'type': oldTypes[i].type};
            }
        }
        console.log(`sendItem2 pressed location=${dataLocation} types= ${JSON.stringify(uploadTypes)}`);
        try {
            firebase.database().ref('bottomsapp/types').update({
                [dataLocation] : uploadTypes
            }, (error) => {
                if (error) {
                    console.log(`got an error=${error}`);
                } else {
                    console.log(`data saved on firebase`);
                    this.handleBackPress();
                }
            });
        } catch(error) {
            console.log(error);
        }
    }
    _keyExtractor = (item, index) => `${item.id}${item.sizeIndex}`;
    renderItem(data) {
        let { item, index } = data;
        //console.log(`item=${JSON.stringify(item)}`);
        return (    
          <View style={styles.cardItem}>
            <TextInput allowFontScaling={false} style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Type"
               value={this.state.types[index].type}
               placeholderTextColor = "#CDCDCD"
               autoCapitalize = "none"
               onChangeText = {(text) => this.handleCategoryChange(text, item, index)}/>   
                <TouchableOpacity style={styles.arrowButton} onPress={() => this.moveItemUp(index)} >
                    <Icon_II name="md-arrow-round-up" size={28} color="#aaaaaa"/>
                </TouchableOpacity>  
                <TouchableOpacity style={styles.arrowButton} onPress={() => this.moveItemDown(index)} >
                    <Icon_II name="md-arrow-round-down" size={28} color="#aaaaaa"/>
                </TouchableOpacity>
          </View>    
        );
      }
    render() {
        const { navigate } = this.props.navigation;
        console.log(`rendering add category`);
        return (
            <SafeAreaView style={styles.safeArea}>
            <View style={[styles.container, ]}>
            <View style={styles.content}>
            <View style={styles.topBar}>
                <View style={styles.topBarContainer}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.handleBackPress()} style={styles.leftContainer}>
                    <Icon_II name="md-arrow-round-back" style={styles.icons}/>
                    <View style={styles.location}>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation1}>
                        Add Category
                        </Text>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation2}>
                        {this.state.locationObj.name}
                        </Text>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.rightContainer}
                    onPress = {
                        () => this.sendItemPressed()
                    }>
                    <Icon_II name="md-cloud-upload" style={styles.iconUpload}/>
                    </TouchableOpacity>
                </View>
            </View>

            {this.state.locationsCategory&&this.state.locationsCategory.length>0&&
            <View style={styles.locationsView}>
              <View style = {styles.scrollViewType}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {
                    this.state.locationsCategory.map((item, index) => (
                    <View key = {item.id} style = {{ flexDirection: 'row', alignItems: 'center',}}>
                    <TouchableOpacity onPress={() => this._pressRow(index)}>
                    <Text allowFontScaling={false} numberOfLines={1} style={[styles.locationText, { color: index==this.state.typeIndex ? '#14cda8' : '#7B8B9B' }]}>{item.name}</Text>
                    </TouchableOpacity>  
                    </View>
                  ))
                }
              </ScrollView>
              </View>
              <View style = {styles.addType} >
              <TouchableOpacity 
               onPress = {() => this.addLocation(navigate)}>
                <Icon_II name="md-add-circle" style={{fontSize:normalize(36), color:'#aaa'}}/>
              </TouchableOpacity>
              </View>
            </View>}
            <ScrollView  style={styles.content}
            keyboardDismissMode='on-drag'>
            {this.state.isLoading&&
            <View style={styles.loading}>
                <ActivityIndicator size='large' color="#14cda8" />
            </View>}
            {this.state.locationsCategory&&this.state.locationsCategory.length>0&&
            <FlatList
                data={this.state.types}
                extraData={this.state}
                keyExtractor={this._keyExtractor}
                renderItem={this.renderItem.bind(this)}
            />}
            <View style={{flex:1, height:300, }}></View>
            </ScrollView>
            </View>  
          </View>
          </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    content: {
        flex: 1,
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#ececec'
    },
    topBar: {
        height: 64,
    },
    arrowButton: {
        justifyContent:'center', 
        padding:12, 
        height: 48,
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    locationsView: {
        flexDirection: 'row',
        height: 64,
    },
    locationText: {
        marginLeft:16,
        marginRight:16,
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(20),
        textAlign: 'center',
    },
    addType: {
        width: 64,
        justifyContent: 'center',
        alignItems: 'center',
    },
    scrollViewType: {
        flex: 28,
    },
    location: {
        flex: 1,
        height: 64,
        flexDirection: 'column',
        paddingLeft: 16,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    topBarContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    leftContainer: {
        flex: 6,
        flexDirection: 'row',
        backgroundColor: '#23395B',
    },
    rightContainer: {
        flex: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#14cda8',
    },
    icons: {
        fontSize: normalize(32),
        color: '#CDCDCD',
        alignSelf: 'center',
        marginLeft: 12,
    },
    iconUpload: {
        fontSize: normalize(36),
        color: '#23395b',
    },
    drinkImage: {
        flex: 1,
        width: null,
        height: null,
    },
    textLocation1: {
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(22),
        color: '#CDCDCD',
        textAlign: 'center',
        flexWrap: 'wrap',
    },
    textLocation2: {
        fontFamily: "RobotoSlab-Bold",
        fontSize: normalize(16),
        color: '#CDCDCD',
        textAlign: 'center',
    },
    input: {
        flex:1,
        paddingLeft: 10,
        alignItems: 'center',
        fontFamily: "RobotoSlab-Light",
        fontSize: normalize(24),
     },
     cardItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:12,
        marginLeft:12,
        marginRight:12,
        borderColor:'#aaaaaa',
        borderWidth:1,
        backgroundColor:'#fff'
      },
});