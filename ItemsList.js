
import React, {Component} from 'react';
import {AsyncStorage, SafeAreaView, Platform, StyleSheet, DeviceEventEmitter, 
  ActivityIndicator, Image, Text, View, ScrollView, FlatList, TouchableOpacity, 
  RefreshControl, NetInfo, InteractionManager } from 'react-native';
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen, RemoteMessage } from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon_II from 'react-native-vector-icons/Ionicons';
import { normalize } from './Utils';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
const placeholder = require('./images/opener1.png');

export default class ItemsList extends Component {
  
  constructor(props) {
    super(props);
    this.addUpdateDrink = this.addUpdateDrink.bind(this);
    this.checkOutAction = this.checkOutAction.bind(this);
    this.handleBackPress = this.handleBackPress.bind(this);
    //disable yellow box image unmounting warning.
    console.disableYellowBox = true;
    console.log(`constructor props1 ItemList`);
    this.state = {
      refreshing: false,
      isAdmin: false,
      isLoading: true,
      isConnected: true,
      isFirstTime: true,
      isEmptyScreen: false,
      types: [],
      data: [],
      fullData: [],
      typeIndex: 1,
      checkoutCost: 0,
      userData: [],
      favouritesKeysData: {},
      userUID: this.props.navigation.state.params ? this.props.navigation.state.params.userUID : '',
      locationObj: this.props.navigation.state.params ? this.props.navigation.state.params.locationObj : '',
      fromLogin: this.props.navigation.state.params ? this.props.navigation.state.params.fromLogin : false,
    };
    console.log('constructor props2 ItemList '+JSON.stringify(this.state.locationObj));
  }
  componentWillUnmount() {
    console.log(`comp will unMount ItemList`);
    if (Platform.OS === "android") {
      this.notificationDisplayedListener();
      this.notificationListener();
      this.notificationOpenedListener();
      this.messageListener();
    }
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }
  componentWillMount() {
    console.log(`comp will mount ItemList`);
    InteractionManager.runAfterInteractions(() => {
      // 2: Component is done animating
      // 3: Start fetching the data
      (async () => {
        await this.fetchAsyncStorage(false);
      })();
    });
    
  }
  async componentDidMount() {
    console.log(`comp did mount ItemList`);
    //this.state.userData.item.index.pop;
    if(Platform.OS !== "android") {
      this.notificationsPermission();
    }
    else {
      this.handleNotifications();
    }
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
  } 
  fetchAsyncStorage = async(didChangeLocation) => {
    
    var locationObj = await AsyncStorage.getItem('locationObj');
    console.log(`location AsyncStorage fetching:${locationObj}`);
    locationObj = locationObj&&JSON.parse(locationObj) ? JSON.parse(locationObj) : this.state.locationObj;
    console.log(`location AsyncStorage fetching:${locationObj}`);
    let dataLocation = '';
    if(locationObj) {
      dataLocation = locationObj.id;
    }
    console.log(`location AsyncStorage fetching:${dataLocation} refreshing=${this.state.isLoading}`);
      
    var userUID = '';
    var isAdmin = '';
    var types = '';
    var tempData = '';
    var userData = '';
    var cost = '';
    var favData = '';

    AsyncStorage.multiGet(['userUUID', 'isAdmin', 'types'+dataLocation, 
      'typesData'+dataLocation, 'userData'+dataLocation, 'checkoutCost'+dataLocation,
      'favouritesKeysData'+dataLocation]).then(response => {
      userUID = JSON.parse(response[0][1]); 
      isAdmin = JSON.parse(response[1][1]); 
      types = JSON.parse(response[2][1]); 
      tempData = JSON.parse(response[3][1]); 
      userData = JSON.parse(response[4][1]); 
      cost = JSON.parse(response[5][1]); 
      favData = JSON.parse(response[6][1]);
      console.log(`userUUID fetchAll:${userUID} isAdmin=${isAdmin} dataLocation=${dataLocation}
      types=${types} tempData=${tempData}`);
      this.setState({ userUID: userUID, locationObj: locationObj, isAdmin: isAdmin,
      types: types, fullData: tempData, userData: userData, checkoutCost: cost ? cost : 0, 
      favouritesKeysData: favData, 
      });
      this.listenForTasks(tempData, didChangeLocation);
    });
  }
  notificationsPermission() {
    firebase.messaging().hasPermission()
      .then(enabled => {
        if (enabled) {
          firebase.messaging().getToken().then(token => {
            console.log("permission token: ", token);
            this.handleNotifications();
          })
          // user has permissions
        } else {
          firebase.messaging().requestPermission()
            .then(() => {
              this.handleNotifications();
            })
            .catch(error => {
              console.log(error);
              // User has rejected permissions  
            });
        }
      });
  }
  handleNotifications() {
    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
      // Process your notification as required
      console.log(`notifi1 : displayed`);
    });
    this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
      // Process your notification as required
      console.log(`notifi2 : displayed`);
      //alert("notification arrived");
    });
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      console.log(`notifi3 : displayed`);
      firebase.notifications().removeAllDeliveredNotifications();

    });
    this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
      // Process your message as required
      console.log(`notifi6 : displayed`);
    });
  }
  handleConnectivityChange = isConnected => {
    console.log(`internet change isConnected=${isConnected} isFirstTime=${this.state.isFirstTime}`);
    if(this.state.isFirstTime) {
      this.setState({isFirstTime: false});
    } else if(isConnected) {
      (async () => {
        await this.listenForTasks();
      })();
    }
  }
  static navigationOptions = ({ navigation }) => ({
    header: null
  });
  _keyExtractor(item, index) {
    return index+'';
  }
  onDonePressList() {
    console.log('Done pressed in list view');
  }
  handleBackPress = (navigate) => {
    console.log(`handing back press ItemList. ${JSON.stringify(this.state.fromLogin)}`);
    if(!this.state.fromLogin) {
      navigate("LoginScreen", {transitionStyle: 'inverted', locationObj: this.state.locationObj, userUID: this.state.userUID, fromListing: true, updateDataFromLogin:this.updateDataFromLogin,});
    }
    else {
      this.props.navigation.goBack();
      this.props.navigation.state.params.updateDataFromListing(this.state.locationObj);
    }    
  }
  _pressRow = (index) => {
    this.setState({ typeIndex: index, isLoading: true });
    this._retrieveData(index);
  }
  
  updateAllTypesData = async(data) => {
    //to do update types data
    try {
      console.log(`data4 and typeIndex=${this.state.typeIndex}}`);
      var types = this.state.types;
      var typeId = types&&types[this.state.typeIndex] ? types[this.state.typeIndex].id : '';
      var tabType = types&&types[this.state.typeIndex] ? types[this.state.typeIndex].type : '';
      console.log(`typeId and tabType=:${typeId} tabType=${(tabType)}`);
      var dataLocation = this.state.locationObj.id;
      if(data&&data[typeId]) {
        //console.log('data8='+JSON.stringify(data));
        await AsyncStorage.setItem('typesData'+dataLocation, JSON.stringify(data));        
        var tempTypeData = data[typeId];
        console.log('data= dontknow');
        var subtypesArray = new Array(Object.keys(tempTypeData).length);
        Object.keys(tempTypeData).map(function(key) {
          console.log('key='+key+' data='+JSON.stringify(tempTypeData[key]));
          subtypesArray[tempTypeData[key].index] = tempTypeData[key];
        });
        console.log('types='+JSON.stringify(subtypesArray));
        this.setState({fullData:data, data: subtypesArray, isLoading: false, refreshing: false});        
      }
      else if(tabType=='Favourites') {
        console.log('favs data getting');
        //to do get favs data from favs keys
        var favsData = this.getDataFromKeys(data, this.state.favouritesKeysData);
        this.setState({fullData:data, data: favsData, isLoading: false, refreshing: false});
      }
      else {
        this.setState({fullData:data, data: '', isLoading: false, isEmptyScreen: true, refreshing: false,});
      }
      if(this.state.data==null) {
        this._retrieveData(this.state.typeIndex);
      }
    } catch (error) {
      console.log(`update error is here=${error.message}`);
    }
  }
  getDataFromKeys(data, favsData) {
    var favouriteData = [];
    //console.log('data='+JSON.stringify(data));
    if(favsData) {
      Object.keys(favsData).map(function(key) {
        console.log('key='+key+' data='+JSON.stringify(favsData[key]));
        var typeId = favsData[key].typeId;
        var subtypeId = favsData[key].subtypeId;
        if(data[typeId]&&data[typeId][subtypeId]) {
          console.log('got favourites from data');
          favouriteData.push(data[typeId][subtypeId]);
        }
      });
    }
    return favouriteData;
  }

  _retrieveData = async (index) => {
    try {
      const dataLocation = this.state.locationObj.id;
      console.log(`_retrieveData location=${dataLocation} types=${this.state.types}`);
      var temTypes = '';
      var temType = '';
      var typeId = '';
      if(this.state.types&&this.state.types[index]) {
        temType = dataLocation+this.state.types[index].type;
        typeId = this.state.types[index].id;
      } 
      console.log('_retrieveData temType='+temType+' typeId='+typeId+
      ' userdata='+JSON.stringify(this.state.userData));
      
      var value = this.state.fullData;
      var userData = this.state.userData;
      if(!userData||userData.length==0) {
        userData = await AsyncStorage.getItem('userdata'+dataLocation);
        if(userData) {
          userData = JSON.parse(userData);
        }
      }
      var cost = this.state.checkoutCost;
      if(!cost) {
        cost = await AsyncStorage.getItem('checkoutCost'+dataLocation);
        if(cost) {
          cost = JSON.parse(cost);
        }
      }
      var favData = this.state.favouritesKeysData;
      if(!favData) {
        favData = await AsyncStorage.getItem('favData'+dataLocation);
        if(favData) {
          favData = JSON.parse(favData);
        }
      }
      //console.log(`value 4=${JSON.stringify(value)} temType=${temType} typeId=${typeId}`);
      //console.log(`value 5=${JSON.stringify(userData)} cost=${cost} 
      //favData=${JSON.stringify(favData)}`);
      
      if(temType===`${dataLocation}Favourites`) {
          console.log(`value+2==${temType}`);
          var isEmpty = favData&&Object.keys(favData).length>0 ? false : true;
          var favsData = this.getDataFromKeys(this.state.fullData, favData);
          console.log('favsData='+favsData);
          this.setState({data: favsData, isLoading: false, 
            userData: userData, checkoutCost: cost ? cost : 0, favouritesKeysData: favData,
              isEmptyScreen: isEmpty});
          
          setTimeout( () => this.refs.listRef ?
            this.refs.listRef.scrollToOffset({ offset: 0, animated: true }) : ''
          , 200);
        }
        else if (value&&value[typeId]) {
          console.log(`value+1==${typeId} uid:${this.state.userUID}`);
          if(this.state.types) {

            var tempTypeData = value[typeId];
            var subtypesArray = new Array(Object.keys(tempTypeData).length);
            Object.keys(tempTypeData).map(function(key) {
              console.log('key='+key+' data='+JSON.stringify(tempTypeData[key]));
              subtypesArray[tempTypeData[key].index] = tempTypeData[key];
            });
            console.log('types='+JSON.stringify(subtypesArray));

            this.setState({data: subtypesArray, 
            userData: userData, checkoutCost: cost ? cost : 0, favouritesKeysData: favData,
              isEmptyScreen: false, isLoading: false});
            if(this.refs.listRef) {
              this.refs.listRef.scrollToOffset({ offset: 0, animated: true });
            }  
            //setTimeout( () => , 1000);
            setTimeout( () => this.refs.listRef ?
            this.refs.listRef.scrollToOffset({ offset: 0, animated: true }) : ''
          , 200);
          } else if(temTypes) {
            this.setState({data: value[temTypes[index].id].data, isLoading: false, userData: userData, 
              checkoutCost: cost ? cost : 0, favouritesKeysData: favData});
          }
        } else {
            console.log(`value+3==${temType}`);
            this.setState({isEmptyScreen: true, isLoading: false, data: '',
            userData: userData, checkoutCost: cost ? cost : 0, favouritesKeysData: favData});
        }
     } catch (error) {
       console.log('error is here='+error.message);
    }
  }
  markFavourite = brandId => {
    var typeId = this.state.types[this.state.typeIndex].id;
    console.log(`brand is here=${brandId} typeId=${typeId}`);
    let tempFavs = this.state.favouritesKeysData ? this.state.favouritesKeysData : {};
    if(tempFavs&&tempFavs[brandId]) {
      delete tempFavs[brandId];
      this.setState({favouritesKeysData: tempFavs});
      console.log('tempFavs is here='+JSON.stringify(tempFavs));
      this.removeFavsFirebase(tempFavs, brandId);
    }
    else {
      var favObj = {'subtypeId': brandId, 'typeId': typeId};
      tempFavs[brandId] = favObj;      
      console.log('tempFavs is here='+JSON.stringify(tempFavs));
      this.setState({favouritesKeysData: tempFavs});
      this.addFavstoFirebase(tempFavs, brandId, typeId);  
    }   
  }
  async removeFavsFirebase(tempFavs, brandId) {
    try {
      var isEmpty = tempFavs&&Object.keys(tempFavs).length>0 ? false : true;
      console.log('firebase data sending...isEmpty='+isEmpty);
      var dataLocation = this.state.locationObj.id;
      firebase.database().ref('bottomsapp/users/' + this.state.userUID+'/'+
      dataLocation+'/favourites/'+brandId).remove();
      
      this.localStoreFavdata(tempFavs, dataLocation);
      var tabType = this.state.types[this.state.typeIndex].type;
      if(tabType=='Favourites') {
        var favsData = this.getDataFromKeys(this.state.fullData, tempFavs);
        this.setState({data: favsData, favouritesKeysData: tempFavs, isEmptyScreen: isEmpty});
      } else {
        this.setState({favouritesKeysData: tempFavs, isEmptyScreen: isEmpty});
      }  
    } catch(error) {
        console.log(error);
    }
  }
  async addFavstoFirebase(tempFavs, brandId, typeId) {
    try {
      //to do send favs data
      var isEmpty = tempFavs ? false : true;
      console.log('firebase data sending...isEmpty='+isEmpty);
      var dataLocation = this.state.locationObj.id;
      var favObj = {'subtypeId': brandId, 'typeId': typeId};
      firebase.database().ref('bottomsapp/users/' + this.state.userUID+'/'+dataLocation+'/favourites').update({
        [brandId]: favObj
        }, function(error) {
        if (error) {
          console.log(`got an error=${error}`);
        } else {
          console.log(`data saved on firebase`);
        }
      });
        
      this.localStoreFavdata(tempFavs, dataLocation);
      var tabType = this.state.types[this.state.typeIndex].type;
      if(tabType=='Favourites') {
        this.setState({data: tempFavs, favouritesKeysData: tempFavs, isEmptyScreen: isEmpty});
      } else {
        this.setState({favouritesKeysData: tempFavs, isEmptyScreen: isEmpty});
      } 
    } catch(error) {
        console.log(error);
    }
  } 
  getTypeIdfromBrandId = (brandId) => {
    var fullData = this.state.fullData;
    var typeId = '';
    Object.keys(fullData).map(function(key) {
      if(fullData[key][brandId]) {
        console.log('got typeId from data='+key);
        typeId = key;
      }
    });
    return typeId;
  } 
  addUserItem = (brandId, sizeIndex, price) => {
    console.log(`clicked8=====${brandId}===${(this.state.checkoutCost)}===${(price)}`);
    var cost = parseInt(this.state.checkoutCost)+parseInt(price);
    var tempdata = this.state.userData ? this.state.userData : {};
    var keyName = brandId+'_'+sizeIndex;
    if(tempdata[keyName]) {
      tempdata[keyName].quantity = tempdata[keyName].quantity+1;    
    }
    else {
      //to do get typeId in favourites also
      //var typeId = this.state.types[this.state.typeIndex].id;
      var typeId = this.getTypeIdfromBrandId(brandId);
      var userItem = { "typeId": typeId, "subtypeId": brandId, "packageIndex": sizeIndex, quantity: 1, };
      tempdata[keyName] = userItem;
    } 
    this.updateUserItemToFirebase(keyName, tempdata, cost);    
  }  
  removeItem = (brandId, sizeIndex, price) => {
    console.log(`clicked9=====${brandId}====${sizeIndex}===${price}`);
    var tempdata = this.state.userData ? this.state.userData : {};
    var keyName = brandId+'_'+sizeIndex;
    var cost = parseInt(this.state.checkoutCost)-parseInt(price);
    if(tempdata[keyName]) {
      if(tempdata[keyName].quantity>1) {
        tempdata[keyName].quantity = tempdata[keyName].quantity-1;
        this.updateUserItemToFirebase(keyName, tempdata, cost);  
      }
      else {
        delete tempdata[keyName];
        this.removeUserItemFromFirebase(keyName, tempdata, cost);
      }
    }   
  }
  async updateUserItemToFirebase(keyName, tempdata, cost) {
    try {
      console.log(`firebase data sending...${JSON.stringify(tempdata[keyName])}`);
      var dataLocation = this.state.locationObj.id;
      firebase.database().ref('bottomsapp/users/' + this.state.userUID + '/' + dataLocation + '/userData').update({
        [keyName]: tempdata[keyName]
      }, function(error) {
        if (error) {
            console.log(`got an error=${error}`);
        } else {
            console.log(`data saved on firebase`);
        }
      });
      cost = cost>0 ? cost : 0;
      this.localStoreUserdata(tempdata, cost, dataLocation);
      this.setState({userData:tempdata, checkoutCost:cost});
    } catch(error) {
      console.log('add item error='+error);
    } 
  }
  async removeUserItemFromFirebase(keyName, tempdata, cost) {
    try {
      console.log(`firebase removing...${JSON.stringify(tempdata[keyName])}`);
      var dataLocation = this.state.locationObj.id;
      firebase.database().ref('bottomsapp/users/' + this.state.userUID + '/' + 
      dataLocation + '/userData/'+keyName).remove();
      cost = cost>0 ? cost : 0;
      this.localStoreUserdata(tempdata, cost, dataLocation);
      this.setState({userData:tempdata, checkoutCost:cost});
    } catch(error) {
      console.log('add item error='+error);
    } 
  }
  categoryValue = (index, value) => {
    var data1 = Object.assign([], this.state.data);
    console.log(`clicked1=====${JSON.stringify(data1[index].selectedSize)}===${value}`);
    data1[index].selectedSize = value;
    this.setState({data:data1});
    //console.log(`clicked2=====${JSON.stringify(this.state.data)}===${value}`);
  }
  //listener to get data from firebase and update listview accordingly
  async listenForTasks(fullData, didChangeLocation=true) {
    console.log('listening for firebase/Internet locationId='+this.state.locationObj.id);
    try {
      NetInfo.isConnected.fetch().then(isConnected => {
        console.log('Internet is ' + (isConnected ? 'online' : 'offline'));
        if(!isConnected) {
          (async () => {
            await this._retrieveData(1);
            if(fullData) {
              console.log('have fullData offline');
              this.updateAllTypesData(fullData);          
            }
          })();
        }
      });
      var dataLocation = this.state.locationObj.id;
      console.log('locationId='+dataLocation);
      firebase.database().ref('bottomsapp/subtypes').on('value', snapshot => {
        if(snapshot.toJSON()) {
          var subTypesData = snapshot.toJSON();
          dataLocation = this.state.locationObj.id;
          console.log('locationId='+dataLocation);
              //console.log('subTypesData='+JSON.stringify(subTypesData));
          DeviceEventEmitter.emit('eventKey', subTypesData);
          this.setState({fullData:subTypesData});
          firebase.database().ref('bottomsapp/users/'+this.state.userUID+'/'+dataLocation).once('value', snapshot => {
            this.localUpdateUserData(snapshot.toJSON(), dataLocation, subTypesData);
          });
          this.updateAllTypesData(this.state.fullData);
        }
      });
      firebase.database().ref('bottomsapp/types').on('value', snapshot => {
        if(snapshot.toJSON()) {
          let typesData = snapshot.toJSON();
          dataLocation = this.state.locationObj.id;
          console.log('typesData='+JSON.stringify(typesData)+' locationId='+dataLocation);
          if(typesData&&typesData[dataLocation]) {
            var localTypes = typesData[dataLocation];
            console.log('types9='+JSON.stringify(localTypes));
            var typesArray = new Array(Object.keys(localTypes).length+1);
            let randomNum = Math.floor(Math.random()*90000) + 10000;                    
            let newId = new Date().getTime()+''+randomNum;
            typesArray[0] = {"id":newId,"type":`Favourites`};
            Object.keys(localTypes).map(function(key) {
              console.log('key='+key+' data='+JSON.stringify(localTypes[key]));
              typesArray[localTypes[key].index+1] = {'id': key, 'type': localTypes[key].type};
            });
            console.log('types='+JSON.stringify(typesArray));
            this.setState({types: typesArray });
            (async() => {
              try {
                console.log('setting types locationwise');
                await AsyncStorage.setItem('types'+dataLocation, JSON.stringify(typesArray));
              } catch (err) {
                console.log('couldnt set types error='+err);
              }
            })();
          } else {
            console.log('snapshot1 is null');
            this.setState({types: [], typeIndex:0, isLoading: false});
          }
          this.updateAllTypesData(this.state.fullData);
          didChangeLocation = false;
        }
      });
      
    } catch(error) {
      console.log(error);
    }
  }
  sortTypes = async(localTypes) => {
    var typesArray = new Array(Object.keys(localTypes).length+1);
    let randomNum = Math.floor(Math.random()*90000) + 10000;                                
    let newId = new Date().getTime()+''+randomNum;
    typesArray[0] = {"id":newId,"type":`Favourites`};
    Object.keys(localTypes).map(function(key) {
      console.log('key='+key+' data='+JSON.stringify(localTypes[key]));
      typesArray[localTypes[key].index+1] = {'id': key, 'type': localTypes[key].type};
    });
    return typesArray;
  }
  async fetchSubTypeData() {
      try {
        console.log(`fetchinf Types : ${JSON.stringify(this.state.types)} index=${this.state.typeIndex}`);
        var secondPostsRef = firebase.database().ref('bottomsapp/subtypes');
        console.log(`secondPostsRef1 : ${secondPostsRef}`);
        secondPostsRef.once('value').then(snapshot => {
          //console.log('data got update'+JSON.stringify(snapshot.toJSON()));
          this.updateAllTypesData(snapshot.toJSON());
        });
      } catch(error) {
          console.log(error);
      }
  }
  async localStoreUserdata(userData, cost, dataLocation) {
    try {
      console.log('local setting user data cost='+cost+' userdata='+JSON.stringify(userData));
      if(userData) {
        await AsyncStorage.setItem('userdata'+dataLocation, JSON.stringify(userData));
      }
      await AsyncStorage.setItem('checkoutCost'+dataLocation, JSON.stringify(cost));
    } catch(error) {
      console.log(error);
    }
  }
  async localStoreFavdata(favData, dataLocation) {
    try {
      console.log('local setting favData data'+JSON.stringify(favData));
      if(favData) {
        await AsyncStorage.setItem('favData'+dataLocation, JSON.stringify(favData));
      }
    } catch(error) {
      console.log(error);
    }
  }
  async fetchUserData(islocationChanged) {
    try {
        var dataLocation = this.state.locationObj.id;
        var secondPostsRef = firebase.database().ref('bottomsapp/users/'+ this.state.userUID + '/' + dataLocation);
        console.log(`secondPostsRef2 : ${secondPostsRef} dataLocation=${dataLocation}`);
        secondPostsRef.once('value').then(snapshot => {
            //this.setState({ data: snapshot.toJSON() });
            console.log(`snapshot data here= : ${JSON.stringify(snapshot.toJSON())}`);
            this.localUpdateUserData(snapshot.toJSON(), dataLocation, this.state.fullData);
            this.fetchSubTypeData(islocationChanged);
        });
      } catch(error) {
          console.log(error);
      }
  }
  localUpdateUserData = (localData, dataLocation, subTypesData) => {
    if(localData!=null) {
      let userData = localData.userData;
      let favs = localData.favourites;
      let totalCost = 0;
      try {
        Object.keys(userData).map(function(key) {
          console.log('key='+key+' data='+JSON.stringify(userData[key]));
          let typeId = userData[key].typeId;
          let subtypeId = userData[key].subtypeId;
          let index = userData[key].packageIndex;
          let quantity = userData[key].quantity;
          let price = 0;
          if(subTypesData[typeId]&&subTypesData[typeId][subtypeId]) {
            price = subTypesData[typeId][subtypeId].packageSize[index].price;
            totalCost += (parseInt(price)*parseInt(quantity));
          }
        });
      } catch(error) {
        console.log('cost calculation error='+error);
      }
      console.log(`userData here= : ${JSON.stringify(userData)} favs:${favs} cost=${totalCost}
      fullData= ${JSON.stringify(subTypesData)}`);
      if(favs!=undefined && userData!=undefined) {
          this.setState({favouritesKeysData: favs, userData:userData, checkoutCost: totalCost});
          this.localStoreFavdata(favs, dataLocation);
          this.localStoreUserdata(userData, totalCost, dataLocation);
      }
      else if(userData!=undefined) {
        this.localStoreUserdata(userData, totalCost, dataLocation);
        this.setState({userData:userData, checkoutCost: totalCost});
      } else if(favs!=undefined) {
        this.setState({favouritesKeysData: favs});
        this.localStoreFavdata(favs, dataLocation);
      } 
    }
    else {
      console.log(`no userdata or favourites`);
      this.setState({ userData: [],
        checkoutCost: 0, favouritesKeysData: [],});
    }
  }
  editDrinkItem = (brandId, navigation, index) => {
    var tempTypes = Object.assign([], this.state.types);
    tempTypes.splice(0, 1);
    console.log(`editing drink brandId= ${brandId} types=${tempTypes}`);
    var item = this.state.data.find(item => item.brandId === brandId);
    console.log(`item=${JSON.stringify(item)} typeIndex=${this.state.typeIndex}`);
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('Internet is ' + (isConnected ? 'online' : 'offline'));
      if(isConnected) {
        navigation.navigate("AddItemScreen", {updateDataAddItem:this.updateDataAddItem, 
          brandObject: item, typeIndex: this.state.typeIndex-1, locationObj: this.state.locationObj,
          types: tempTypes, index: index, fullData: this.state.fullData });
      } else {
        alert('Please connect to the internet');
      }
    });   
  }
  swapItems = (obj, prop1, prop2) => {
    var tmp = obj[prop1];
    obj[prop1] = obj[prop2];
    obj[prop2] = tmp;
  }
  moveItemDown = (brandId, index) => {
    let typeVar = this.state.types[this.state.typeIndex].id.toString();
    console.log(`item ${brandId} down ${index} ${typeVar}`);
    if (index < this.state.data.length - 1) {
      let tempData = [...this.state.data];
      var brandId2 = tempData[index+1].brandId;
      console.log(`item brandId2 ${tempData[index+1].brandId} down ${index+1}`);
      tempData[index].index = index+1;
      tempData[index+1].index = index;
      //this.swapItems(tempData, index, index + 1);
      this.setState({data: tempData});
      this.sendSortedDatatoFirebase(typeVar, brandId, index+1, brandId2, index);
    }
  }
  moveItemUp = (brandId, index) => {
    let typeVar = this.state.types[this.state.typeIndex].id.toString();
    console.log(`item ${brandId} up ${index} ${typeVar}`);
    if(index>0) {
      let tempData = [...this.state.data];
      var brandId2 = tempData[index-1].brandId;
      console.log(`item brandId2 ${brandId2} up ${index-1}`);
      tempData[index].index = index-1;
      tempData[index-1].index = index;
      //this.swapItems(tempData, index, index - 1);
      this.setState({data: tempData});
      this.sendSortedDatatoFirebase(typeVar, brandId, index-1, brandId2, index);
    }
  }
  async sendSortedDatatoFirebase(type, brandId1, index1, brandId2, index2) {
    try {
      //to do sorted data change
      var updatedUserData = {};
      updatedUserData[brandId1+'/index'] = index1;
      updatedUserData[brandId2+'/index'] = index2;
      console.log(`firebase data sending...${type} data=${JSON.stringify(updatedUserData)}`);
      firebase.database().ref('bottomsapp/subtypes/'+type).update(
        updatedUserData
      , function(error) {
        if (error) {
            console.log(`got an error=${error}`);
        } else {
            console.log(`data saved on firebase`);
        }
      });
    } catch(error) {
      console.log(error);
    } 
  }
  renderTypes = (data) => {
    let { item, index } = data;
    console.log('rendering types='+index);
    return(
    <TouchableOpacity activeOpacity={0.9} onPress={() => this._pressRow(index)}
                     key = {item.id} style = {{ flexDirection: 'row', alignItems: 'center',}}>
                    <View>
                    {item.type=='Favourites' 
                    ? <Icon name="star" style={[styles.favouritesCategory, {color: index==this.state.typeIndex ? '#14cda8' : '#23395B'}]}/>
                    : <Text allowFontScaling={false} numberOfLines={1} style={[styles.categoryText, { color: index==this.state.typeIndex ? '#14cda8' : '#23395B' }]}>{item.type}</Text>
                    }
                    </View>  
    </TouchableOpacity>
    );
  }
  renderItem = (data) => {
    let { item, index } = data;
    //console.log('packageSize='+JSON.stringify(item));
    let item1 = item.packageSize;
    var userdata = this.state.userData; 
    console.log('userdata='+userdata);
    var itemAdded1 = userdata ? userdata[item.brandId+'_'+0] : undefined;
    var itemAdded2 = userdata ? userdata[item.brandId+'_'+1] : undefined;
    var itemAdded3 = userdata ? userdata[item.brandId+'_'+2] : undefined;
    //console.log(`clicked3===${(JSON.stringify(item))}==${(itemAdded1)}===${this.state.types[this.state.typeIndex].type.toString()}`);
    var isFavourite = this.state.types[this.state.typeIndex].type.toString()=='Favourites';
    return (
      <View style={styles.listItem}>
        <View style={styles.listItemHeader}>
          <Text allowFontScaling={false} numberOfLines={1} style={styles.listItemName}>{item.brandName}</Text>
          {!isFavourite&&this.state.isAdmin&&  
          <TouchableOpacity onPress={() => this.moveItemUp(item.brandId, index)} >
            <Icon_II name="md-arrow-round-up" size={28} color="#aaaaaa" style={{marginRight:18}}/>
          </TouchableOpacity>
          }
          {!isFavourite&&this.state.isAdmin&& 
          <TouchableOpacity onPress={() => this.moveItemDown(item.brandId, index)} >
            <Icon_II name="md-arrow-round-down" size={28} color="#aaaaaa" style={{marginRight:18}}/>
          </TouchableOpacity>
          }
          {!isFavourite&&this.state.isAdmin&& 
          <TouchableOpacity onPress={() => this.editDrinkItem(item.brandId, this.props.navigation, index)} >
            <Icon_II name="md-create" size={28} color="#aaaaaa" style={{marginRight:18}}/>
          </TouchableOpacity>
          }
          <TouchableOpacity onPress={() => this.markFavourite(item.brandId)} >
            <Icon name="star" size={28} style={{color: this.state.favouritesKeysData&&this.state.favouritesKeysData[item.brandId] ? '#14cda8' : '#aaaaaa'}}/>
          </TouchableOpacity>
        </View>
        <View  style={{ borderBottomColor: '#aaaaaa', borderBottomWidth: 1, }} />
        <View style={styles.listItemBody}>
          <View style={styles.listItemImage}>
            <Image source={{uri: item.brandImage, cache: 'force-cache',}} style={styles.itemImage}
              defaultSource={placeholder} 
              onError={(error)=> console.log('error in image loading'+error)}
            />
          </View>
          <View style={styles.listItemCategory}>
            
            {   item1[0] &&
              <TouchableOpacity activeOpacity={0.9} onPress={() => this.categoryValue(index, 0)} style={{flex: 1, backgroundColor: item.selectedSize==0 ? '#dddddd' : '#e5e5e5',}}>
                <View style={{flex:1, paddingLeft: 10, justifyContent: 'center',}}>
                <Text allowFontScaling={false} numberOfLines={1} style={[styles.textCategory, { color: item.selectedSize==0 ? '#23395b' : '#999999', fontSize: normalize(16)}]}>
                {item1[0].name}
                </Text>
                <Text allowFontScaling={false} numberOfLines={1} style={[styles.textCategory, {color: item.selectedSize==0 ? '#23395b' : '#999999', }]}>
                  Rs.{item1[0].price}{itemAdded1!=undefined ? ` x ${itemAdded1.quantity} = ${item1[0].price*itemAdded1.quantity}` : ""}
                </Text>
                </View>
              </TouchableOpacity>
            }
            {<View  style={{ borderBottomColor: '#aaaaaa', borderBottomWidth: item1[1] ? 1 : 0, }} />}
            {
                item1[1] &&
                <TouchableOpacity activeOpacity={0.9} onPress={() => this.categoryValue(index, 1)} style={{flex: 1, backgroundColor: item.selectedSize==1 ? '#dddddd' : '#e5e5e5',}}>
                    <View style={{flex:1, paddingLeft: 10, justifyContent: 'center',}}>
                    <Text allowFontScaling={false} numberOfLines={1} style={[styles.textCategory, {color: item.selectedSize==1 ? '#23395b' : '#999999', fontSize: normalize(16)}]}>
                    {item1[1].name}
                    </Text>
                    <Text allowFontScaling={false} numberOfLines={1} style={[styles.textCategory, {color: item.selectedSize==1 ? '#23395b' : '#999999', }]}>
                    Rs.{item1[1].price}{itemAdded2!=undefined ? ` x ${itemAdded2.quantity} = ${item1[1].price*itemAdded2.quantity}` : ""}
                    </Text>
                    </View>
              </TouchableOpacity>
            }
            {<View  style={{ borderBottomColor: '#aaaaaa', borderBottomWidth: item1[2] ? 1 : 0, }} />}
            {
                item1[2] &&
                <TouchableOpacity activeOpacity={0.9} onPress={() => this.categoryValue(index, 2)} style={[styles.listItemCategory2, {backgroundColor: item.selectedSize==2 ? '#dddddd' : '#e5e5e5',}]}>
                  <View style={{flex:1, paddingLeft: 10, justifyContent: 'center',}}>
                    <Text allowFontScaling={false} numberOfLines={1} style={[styles.textCategory, {color: item.selectedSize==2 ? '#23395b' : '#999999', fontSize: normalize(16)}]}>
                    {item1[2].name}
                    </Text>
                    <Text allowFontScaling={false} numberOfLines={1} style={[styles.textCategory, {color: item.selectedSize==2 ? '#23395b' : '#999999', }]}>
                    Rs.{item1[2].price}{itemAdded3!=undefined ? ` x ${itemAdded3.quantity} = ${item1[2].price*itemAdded3.quantity}` : ""}
                    </Text>
                  </View>
              </TouchableOpacity>
            }
          </View>
          <View style={styles.listItemSide}>
            
            <TouchableOpacity activeOpacity={0.6} onPress={() => this.addUserItem(item.brandId, item.selectedSize, item1[item.selectedSize].price)} style={{flex:1,}}>
              <Icon_II name='md-add' size={36} color="#aaaaaa" style={styles.listItemOuterSidePlus}/>
            </TouchableOpacity>    
            <View style={styles.horizontalLine} />        
            <TouchableOpacity activeOpacity={0.6} onPress={() => this.removeItem(item.brandId, item.selectedSize, item1[item.selectedSize].price)} style={{flex:1,}}>
              <Icon_II name='md-remove' size={36} color="#aaaaaa" style={styles.listItemOuterSidePlus}/>
            </TouchableOpacity>     
          </View>
        </View>
      </View>
    ) 
  }
  updateDataAddItem  = (typeIndex) => {
    console.log('updating1 user data coming from : '+this.state.types);
  };
  updateDataFromLogin  = (userId, location) => {
    console.log('updating user data coming from login :'+JSON.stringify(location)+' old='+this.state.locationObj.name);
    var didChangeLocation = location.name==this.state.locationObj.name ? false : true;
    if(didChangeLocation||userId!=this.state.userUID) {
      console.log('location changed');
      (async () => {
        this.setState({userUID:userId, data: [], isLoading: true, location: location,
          userData: [], checkoutCost: 0, favouritesKeysData: [], types: [], typeIndex: 1,
            isEmptyScreen: false, isFirstTime: true })
        await this.fetchAsyncStorage(didChangeLocation);
      })();
      
    } 
  };
  updateDataFromCheckout  = (checkoutCost, userData) => {
    console.log('updating user data coming from checkout : '+checkoutCost+' userdata='+JSON.stringify(userData));
    this.setState({userData:userData, checkoutCost: checkoutCost ? checkoutCost : 0});
    //alert('come back status: '+data);
  };
  addUpdateDrink = (navigate) => {
    var tempTypes = Object.assign([], this.state.types);
    tempTypes.splice(0, 1);
    console.log(`navigating to add/update drink by admin types=${tempTypes}`);
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('Internet is ' + (isConnected ? 'online' : 'offline'));
      if(isConnected) {
        navigate("AddItemScreen", {updateDataAddItem:this.updateDataAddItem, 
          typeIndex: this.state.typeIndex-1, locationObj: this.state.locationObj,
          types: tempTypes, fullData: this.state.fullData});
      } else {
        alert('Please connect to the internet');
      }
    });  
  }
  checkOutAction = (navigate) => {
    console.log(`checkout clicked`);
    navigate("CheckoutScreen", {updateDataFromCheckout:this.updateDataFromCheckout, 
      fulldata:this.state.fullData, userUID:this.state.userUID, cartData: this.state.userData, 
      locationObj: this.state.locationObj, types: this.state.types});
  }
  onRefresh = () => {
    console.log('user refresh action');
    this.setState({refreshing: true});
    this.fetchUserData(false);
  }
  componentDidUpdate() {
    console.log('comp did update called');
    
  }
  render() {
    const { navigate } = this.props.navigation;
    console.log(`userID is ${this.state.userUID} and=
     types=${JSON.stringify(this.state.types)} isEmpty=${this.state.isEmptyScreen}`);
    //console.log(`clicked4=====${JSON.stringify(this.state.data)}===`);
  

  return (
    <SafeAreaView style={styles.safeArea}>
    <View style={[styles.topView, ]}>
        <View style={styles.topBar}>
          <View style={styles.container}>
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.handleBackPress(navigate)} style={styles.leftContainer}>
              <Image style={styles.appIcon} source={require('./images/opener.png')} />
              <View style={styles.location}>
                <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation1}>
                  BottomsApp
                </Text>
                <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation2}>
                  {this.state.locationObj.name}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.checkOutAction(navigate)} style={styles.rightContainer}>
              <Text allowFontScaling={false} style={styles.checkoutValue}>Rs. {this.state.checkoutCost}    </Text>
              <Icon_II name='md-cart' style={styles.iconBasket}/>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.category}>
        <View style = {styles.scrollViewType}>
        
          <FlatList 
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          initialNumToRender={0}
          ref="scrollRef"
          keyExtractor={this._keyExtractor}
          extraData={this.state.typeIndex}  
          data={this.state.types}
          renderItem={this.renderTypes} 
          navigation={this.props.navigation}       
          />
              
        </View>
        {this.state.isAdmin&&  
              <View style = {styles.addType} >
              <TouchableOpacity 
               onPress = {() => this.addUpdateDrink(navigate)}>
                <Icon_II name='md-add-circle' style={{fontSize:normalize(36), color:'#aaa'}}/>
              </TouchableOpacity>
              </View>
        }
        </View>
        {!this.state.isLoading
        ?
        <View style={styles.listContent}>
        {this.state.isEmptyScreen&&(!this.state.data||this.state.data.length==0) 
        ?
          <View style={styles.emptyContent}>
            <View style={{flex:2, }}></View>
            <Text allowFontScaling={false} style={styles.emptyContentText}>Nothing Here.</Text>
          </View>
        :
        <FlatList 
          refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
            tintColor="#14cda8" 
            titleColor="#14cda8" 
          />
          }
          initialNumToRender={0}
          ref="listRef"
          keyExtractor={this._keyExtractor}
          data={this.state.data}
          renderItem={this.renderItem} 
          extraData={this.state}  
          navigation={this.props.navigation}       
        />
        }
        </View>
        :
          <View style={styles.loading}>
                <ActivityIndicator size='large' color="#14cda8" />
            </View>}
        
      </View>
      </SafeAreaView>

    );
  }
}

const styles = StyleSheet.create({
  topView: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ececec',
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#ececec'
  },
  topBar: {
    height: 64,
  },
  category: {
    height: 64,
    flexDirection: 'row',
  },
  scrollViewType: {
    flex: 28,
    marginLeft:8,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  addType: {
    width: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listContent: {
    flex: 1,
    backgroundColor: '#ececec',
  },
  emptyContent: {
    flex: 1,
    backgroundColor: '#ececec',
  },
  emptyContentText: {
    flex:6,
    fontSize: 60,
    marginLeft: 12,
    marginRight: 12,
    textAlign: 'center',
    fontFamily: 'RobotoSlab-Bold',
    color: '#cccccc',
    textAlign: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  leftContainer: {
    flex: 6,
    flexDirection: 'row',
    backgroundColor: '#23395B',
  },
  rightContainer: {
    flex: 4,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#14cda8',
  },
  location: {
    flex: 1,
    height: 64,
    flexDirection: 'column',
    paddingLeft: 16,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  textLocation1: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(22),
    color: '#CDCDCD',
    textAlign: 'center',
  },
  textLocation2: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(16),
    color: '#CDCDCD',
    textAlign: 'center',
  },
  textCategory: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(14),
    flexWrap: 'wrap',
  },
  icons: {
    fontSize: normalize(40),
    color: '#CDCDCD',
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft:12,
  },
  appIcon: {
    width: 21,
    height: 40,
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft:12,
  },
  favouritesCategory : {
    fontSize: normalize(28),
    padding : 8,
  },
  iconBasket: {
    flex: 3,
    fontSize: normalize(36),
    color: '#23395b',
    paddingRight: 8,
  },
  checkoutValue: {
    flex: 8,
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(18),
    color: '#23395b',
    paddingLeft: 12,
  },
  categoryText: {
    marginLeft:16,
    marginRight:16,
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(20),
    textAlign: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#2265E4',
    color: '#7B8B9B',
    marginBottom: 5,
  },
  listItem: {
    marginBottom: 16,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    paddingBottom: 10,
    height: 220,
    borderWidth: 1,
    borderColor:'#aaaaaa',
  },
  listItemHeader: {
    flexDirection: 'row',
    height: 48,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingLeft: 8,    
    paddingRight: 8,
    backgroundColor: '#dddddd',
  },
  listItemBody: {
    height: 169,
    flexDirection: 'row',  
  },
  iconStar: {
    fontSize: normalize(28),
    color: '#CDCDCD',
  },
  listItemName: {
    flex: 1,
    color: '#23395B',
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(20),
    justifyContent: 'flex-start',
  },
  listItemImage: {
    flex: 4,
  },
  itemImage: {
    flex: 1
  },
  listItemCategory: {
    flex: 4,
    justifyContent: 'space-between',
    borderLeftWidth: 1,
    borderColor:'#aaaaaa',
    backgroundColor: '#000'
  },
  listItemCategory2: {
    flex: 1,
    justifyContent: 'center',
  },
  listItemSide: {
    flex: 2,
    flexDirection: 'column',
    borderLeftWidth: 1,
    borderColor:'#aaaaaa',
    backgroundColor: '#dddddd'
  },
  listItemOuterSidePlus: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  horizontalLine: {
    borderBottomColor: '#aaaaaa', 
    borderBottomWidth: 1,
  }
});
