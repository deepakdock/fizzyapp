/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { AsyncStorage, Alert, SafeAreaView, DeviceEventEmitter, FlatList, NetInfo, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase';
import Icon_II from 'react-native-vector-icons/Ionicons';
import { normalize } from './Utils';

var subscription = '';

export default class Checkout extends Component {
  static navigationOptions = ({ navigation }) =>  ({
    headerTitle: "Checkout Title",
    headerTintColor: '#000',
    headerTitleStyle: { color: '#000' },
    headerLeft: (
      <TouchableOpacity onPress={() => {
          console.log('handling 2back press.');
          navigation.goBack();
          navigation.state.params.updateDataFromCheckout(this.state.checkoutCost);
      }} >
        <Icon_II name="md-arrow-round-back" size={28} color="#2a2a2a" style={{marginLeft:8}}/>
      </TouchableOpacity>
    ),
  });
  constructor(props) {
    super(props);  
    this.state = {
      cartData: this.props.navigation.state.params.cartData,
      fulldata: this.props.navigation.state.params.fulldata,
      displayData: [],
      userUID: this.props.navigation.state.params.userUID,
      types: this.props.navigation.state.params.types,
      isFirstTime: true,
      checkoutCost: 0,
      quantity: 0,
      locationObj: this.props.navigation.state.params.locationObj,
    };
    console.log('constructor props1 Checkout');
  }
  //updating cartData values according to item data          
  getDataFromKeys(fulldata, cartData) {
    var displayData = [];
    let quantity=0;
    let cost=0;
    console.log(`location Checkout:${JSON.stringify(this.state.locationObj.name)}
      fulldata=${JSON.stringify(this.state.fulldata)}`);
    Object.keys(cartData).map(function(key) {
      console.log('key='+key+' data='+JSON.stringify(cartData[key]));
      var cartItem = cartData[key];
      var subtypeId = cartItem.subtypeId;
      var typeId = cartItem.typeId;
      if(fulldata[typeId]&&fulldata[typeId][subtypeId]) {
        var dataItem = fulldata[typeId][subtypeId];
        var packageIndex = cartItem.packageIndex;
        displayData.push({'brandName':dataItem.brandName, 'id':cartItem.subtypeId, 
        'price':dataItem.packageSize[packageIndex].price, 'quantity':cartItem.quantity,
        'sizeIndex':packageIndex, 'sizeName':dataItem.packageSize[packageIndex].name});

        quantity += cartItem.quantity;
        cost += cartItem.quantity*dataItem.packageSize[packageIndex].price; 
      }
    });
    this.setState({displayData: displayData, checkoutCost:cost, quantity: quantity});
    console.log('display='+JSON.stringify(displayData));
  }
  //on back press update cartData with updated values and localStore update and send back to Listing Screen
  handleBackPress() {
    console.log('handing back2 press Checkout');
    var userData = this.state.cartData;
    let tempData = {};
    if(userData) {
      Object.keys(userData).map(function(key) {
          console.log('key='+key+' data='+JSON.stringify(userData[key]));
          var cartItem = userData[key];
          if(cartItem.quantity>0) {
            tempData[key] = cartItem;
          }
      });
    }
    (async () => {
      await this.localStoreUserdata(tempData, this.state.checkoutCost, this.state.locationObj.id);
    })();
    this.props.navigation.goBack();
    this.props.navigation.state.params.updateDataFromCheckout(this.state.checkoutCost, tempData);
  }
  _keyExtractor = (item, index) => `${item.id}${item.sizeIndex}`;

  async localStoreUserdata(userData, cost, dataLocation) {
    try {
      console.log('local setting user data cost');
      if(userData) {
        await AsyncStorage.multiSet([
          ['userdata'+dataLocation,JSON.stringify(userData)],
         ['checkoutCost'+dataLocation, JSON.stringify(cost)]
        ]);
      }
      else {
        await AsyncStorage.setItem('checkoutCost'+dataLocation, JSON.stringify(cost));
      }
    } catch(error) {
      console.log(error);
    }
  }

  addItem = (drinkItem) => {
    console.log(`clicked8=====${drinkItem.id}===${(this.state.checkoutCost)}===${(drinkItem.price)}`);
    var cost = parseInt(this.state.checkoutCost)+parseInt(drinkItem.price);
    var tempdata = this.state.cartData ? this.state.cartData : {};
    var keyName = drinkItem.id+'_'+drinkItem.sizeIndex;
    console.log('tempData='+JSON.stringify(tempdata));
    if(tempdata[keyName]) {
      tempdata[keyName].quantity = tempdata[keyName].quantity+1;    
      this.updateUserItemToFirebase(keyName, tempdata, cost, this.state.quantity+1);   
    }
  }
  
  removeItem = (drinkItem) => {
    console.log(`clicked9=====${drinkItem.id}====${drinkItem.sizeIndex}===${drinkItem.price}`);
    var tempdata = this.state.cartData ? this.state.cartData : {};
    var keyName = drinkItem.id+'_'+drinkItem.sizeIndex;
    var cost = parseInt(this.state.checkoutCost)-parseInt(drinkItem.price);
    if(tempdata[keyName]) {
      if(tempdata[keyName].quantity>0) {
        tempdata[keyName].quantity = tempdata[keyName].quantity-1;
        if(tempdata[keyName].quantity>0) {
          console.log('subtracting1 quantity more than 1 items');
          this.updateUserItemToFirebase(keyName, tempdata, cost, this.state.quantity-1); 
        } else {
          console.log('subtracting2 quantity on zero items');
          this.getDataFromKeys(this.state.fulldata, tempdata);
          this.setState({cartData:tempdata, checkoutCost:cost>0 ? cost : 0, 
            quantity: this.state.quantity-1});
          this.removeUserItemFromFirebase(keyName, tempdata, cost>0 ? cost : 0);
        }
      }
      else {
        //no need to update as removeing from 0 items
        console.log('deleting cartdata from firebase');
      }
    } 
  }
  async updateUserItemToFirebase(keyName, tempdata, cost, quantity) {
    try {
      console.log(`firebase cartItem sending...${JSON.stringify(tempdata[keyName])}`);
      var dataLocation = this.state.locationObj.id;
      firebase.database().ref('bottomsapp/users/' + this.state.userUID + '/' + dataLocation + '/userData').update({
        [keyName]: tempdata[keyName]
      }, function(error) {
        if (error) {
            console.log(`got an error=${error}`);
        } else {
            console.log(`data saved on firebase`);
        }
      });
      cost = cost>0 ? cost : 0;
      this.getDataFromKeys(this.state.fulldata, tempdata);
      this.setState({cartData:tempdata, checkoutCost:cost>0 ? cost : 0, quantity: quantity});
    } catch(error) {
      console.log('add item error='+error);
    } 
  }
  async removeUserItemFromFirebase(keyName, tempdata, cost) {
    try {
      let tempObj = Object.assign({}, tempdata);
      delete tempObj[keyName];
      console.log(`firebase cartItem removing...${JSON.stringify(tempdata[keyName])}`);
      var dataLocation = this.state.locationObj.id;
      firebase.database().ref('bottomsapp/users/' + this.state.userUID + '/' + 
      dataLocation + '/userData/'+keyName).remove();
    } catch(error) {
      console.log('add item error='+error);
    } 
  }

  async componentWillMount() {
    console.log(`datacheckout=${this.state.cartData} and UID=${this.state.userUID}`);
    this.getDataFromKeys(this.state.fulldata, this.state.cartData);   
  }
  handleConnectivityChange = isConnected => {
    console.log(`internet change isConnected=${isConnected}`);
    if(isConnected) {
      this.listenForTasks();
    }
  }
  async componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    this.listenForTasks();
    subscription = DeviceEventEmitter.addListener('eventKey', (e) => {
      console.log('emitted event came');
      this.getDataFromKeys(e, this.state.cartData);
    });
  }
  componentWillUnmount() {
    console.log(`comp will unMount Checkout`);
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    //DeviceEventEmitter.removeAllListeners();
    subscription.remove(); // Removes the subscription
  }
  //listener to get data from firebase and update listview accordingly
  listenForTasks() {
    console.log('listening for firebase/Internet');
    try {
      if(!this.state.isFirstTime) {
        firebase.database().ref('bottomsapp/subtypes').on('value', snapshot => {
          console.log('data got update listener checkout'+JSON.stringify(snapshot.toJSON()));
          this.getDataFromKeys(snapshot.toJSON(), this.state.cartData);
        });
      } else {
        this.setState({isFirstTime: false});
      }
    } catch(error) {
      console.log(error);
    }
  }
  renderItem(data) {
    let { item, index } = data;
    console.log(`item=${JSON.stringify(item)}`);
    return (    
      <View style={styles.cardItem}>
        <View style={styles.nameView}>
          <Text allowFontScaling={false} numberOfLines={1} style={{ fontFamily: "RobotoSlab-Bold", fontSize:normalize(18), color: '#23395b',}}>{item.brandName}</Text>
          <Text allowFontScaling={false} numberOfLines={1} style={{ fontFamily: "RobotoSlab-Bold", color: '#23395b'}}>{item.sizeName}</Text>
          <Text allowFontScaling={false} numberOfLines={1} style={{ fontFamily: "RobotoSlab-Bold", fontSize:normalize(18), color: '#23395b'}}>{item.quantity} x Rs.{item.price} = {item.quantity*item.price}</Text>
        </View>
        <View style={styles.minusView}>
          <TouchableOpacity onPress={() => this.removeItem(item)} >
            <Icon_II name="md-remove-circle" size={44} color="#aaa" style={{}}/>
          </TouchableOpacity>
        </View>
        <View style={styles.plusView}>
        <TouchableOpacity onPress={() => this.addItem(item)} >
            <Icon_II name="md-add-circle" size={44} color="#aaa" style={{}}/>
          </TouchableOpacity>
        </View>       
      </View>    
    );
  }
  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
      <View style={[styles.container, ]}>
        <View style={styles.topBar}>
                <View style={styles.topBarContainer}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.handleBackPress()} style={styles.leftContainer}>
                    <Icon_II name="md-arrow-round-back" style={[styles.icons, {paddingLeft: 16}]}/>
                    <View style={styles.location}>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation1}>
                        BottomsApp
                        </Text>
                        <Text allowFontScaling={false} numberOfLines={1} style={styles.textLocation2}>
                        {this.state.locationObj.name}
                        </Text>
                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8}
                     style={styles.rightContainer} onPress={() => Alert.alert('Sorry','We are not serving currently')} >
                      <Text allowFontScaling={false} style={styles.textCheckout}>Rs. {this.state.checkoutCost}    </Text>
                      <Icon_II name='md-cash' style={{flex: 3,fontSize: normalize(36), 
                         color: '#23395b', paddingRight: 8,}}/>
                    </TouchableOpacity>
                </View>
        </View> 
        <FlatList
          data={this.state.displayData}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this.renderItem.bind(this)}
        />
        <View style={styles.footer}>
          <View style={styles.totalItem}>
            <Text allowFontScaling={false} style={{ fontFamily: "RobotoSlab-Bold", color: '#23395B', fontSize: normalize(22)}}>
            {this.state.quantity} Items - Rs. {this.state.checkoutCost}</Text>
          </View>
        </View>
      </View>
      </SafeAreaView>
    );
  }
}
// F08080 008B8B
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ececec',
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#ececec'
  },
  topBar: {
    height: 64,
    marginBottom: 12,
  },
  topBarContainer: {
      flex: 1,
      flexDirection: 'row',
  },
  leftContainer: {
      flex: 6,
      flexDirection: 'row',
      backgroundColor: '#23395B',
  },
  rightContainer: {
      flex: 4,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#14cda8',
  },
  location: {
    flex: 1,
    height: 64,
    flexDirection: 'column',
    paddingLeft: 16,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  icons: {
    fontSize: normalize(32),
    color: '#CDCDCD',
    alignSelf: 'center',
  },
  textCheckout: {
    flex: 8,
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(18),
    color: '#23395b',
    paddingLeft: 12,
  },
  textLocation1: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(22),
    color: '#CDCDCD',
    textAlign: 'center',
    flexWrap: 'wrap',
  },
  textLocation2: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(16),
    color: '#CDCDCD',
    textAlign: 'center',
  },
  iconUpload: {
    fontSize: normalize(36),
    color: '#CDCDCD',
  },
  totalText: {
    flex: 5,
    justifyContent: 'center',
    textAlign: 'center', 
    alignItems: 'center',
    backgroundColor: '#F5DEB3'
  },
  totalItem: {
    flex: 1,
    justifyContent: 'center',
    textAlign: 'center', 
    alignItems: 'center',
    backgroundColor: '#14cda8'
  },
  totalCost: {
    flex: 3,
    justifyContent: 'center',
    textAlign: 'center', 
    alignItems: 'center',
    backgroundColor: '#F5DEB3'
  },
  footer: {
    height: 64,
    flexDirection: 'row',
  },
  cardItem: {
    flex: 1,
    height: 80,
    flexDirection: 'row',
    backgroundColor: '#dddddd',
    marginLeft: 12,
    marginRight: 12,
    marginBottom: 12,
    textAlign: 'center',
    borderWidth: 1,
    borderColor:'#aaaaaa',
  },
  nameView: {
    flex: 8,
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: 8,
    alignItems: 'flex-start',
    backgroundColor: '#dddddd',
  },
  minusView: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  quantityView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  plusView: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  costView: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
