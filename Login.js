/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {PixelRatio, NetInfo, AsyncStorage, SafeAreaView, TextInput, Platform, Picker, 
  ActivityIndicator, StyleSheet, Text, View, Image, TouchableOpacity, WebView} from 'react-native';
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen, RemoteMessage } from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import { AccessToken, LoginManager, LoginButton } from 'react-native-fbsdk';
import { normalize } from './Utils';
import {GoogleSignin} from 'react-native-google-signin';
import Geocoder from 'react-native-geocoder';
import Button from 'react-native-button';
import ModalSelector from 'react-native-modal-selector';
import Icon_II from 'react-native-vector-icons/Ionicons';
 
export default class Login extends Component {

  compVar = this;
  static navigationOptions = {
    header: null
  };
  
  constructor(props) {
    super(props);
    console.log(`constructor called pixelration=${PixelRatio.get()}`);
    this.unsubscriber = null;    
    this.state = {
      user: null,
      locationObj: this.props.navigation.state.params ? this.props.navigation.state.params.locationObj : {},
      userLocation: null,
      locationsArray: [],
      checked: false,
      isLoading: false,
      loginEnabled: true,
      checkingTerms: false,
      userUID: this.props.navigation.state.params ? this.props.navigation.state.params.userUID : null,
      fromListing: this.props.navigation.state.params ? this.props.navigation.state.params.fromListing : false,
    };
    this.onLoginFacebook = this.onLoginFacebook.bind(this);
    this.onLoginGoogle = this.onLoginGoogle.bind(this);
    this.checkUserDetails = this.checkUserDetails.bind(this);
  }
  componentWillMount() {
    this.getUserUID();
  }
  getUserUID = async() => {
    try {
      const value = await AsyncStorage.getItem('userUUID');
      const location = await AsyncStorage.getItem('locationObj');
      console.log(`comp will mount Login value=${JSON.parse(value)} location=${JSON.parse(location)}`);
      this.setState({userUID: JSON.parse(value), locationObj: location ? JSON.parse(location) : {}, 
        isLoading: this.state.fromListing});
      this.fetchLocations();
    } catch (error) {
      // Error retrieving data
      console.log(`error is here=${error.message}`);
    }
  }
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(`geo 1data= ${position.coords.latitude} === ${position.coords.longitude}`);
        
        Geocoder.geocodePosition({lat: position.coords.latitude, lng: position.coords.longitude})
        .then(res => {
          console.log(`geo 2data= ${JSON.stringify(res)}`);
          console.log(`geo 2data= ${JSON.stringify(res[0].subLocality)}`);
          this.setState({
            userLocation: res[0].subLocality,
          });
        })
        .catch(error => console.warn(error));
        
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 3000 },
    );
    this.unsubscriber = firebase.auth().onAuthStateChanged((changedUser) => {
          // console.log(`changed User : ${JSON.stringify(changedUser.toJSON())}`);
          this.setState({ user: changedUser });
    });
    GoogleSignin.configure({
        iosClientId: '253027260613-2fo9v5a7n20slvv80s8pc0pur97rdt52.apps.googleusercontent.com', // only for iOS
    });
    if (Platform.OS === "android") {
      firebase.messaging().hasPermission()
      .then(enabled => {
        if (enabled) {
          // user has permissions
          console.log(`geo user has permission`);
        } else {
          // user doesn't have permission
          console.log(`geo user no permission`);
        } 
      });
      this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
        // Process your notification as required
        console.log(`notifi1 : displayed`);
      });
      this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
        // Process your notification as required
        console.log(`notifi2 : displayed ${notification}`);
        if (Platform.OS === "android") {
          notification.android.setChannelId("forgroundnotification");
        }
        firebase.notifications().displayNotification(notification);
      });
      this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
        // Get the action triggered by the notification being opened
        console.log(`notifi3 : displayed`);
        const action = notificationOpen.action;
        console.log(`notifi4 : displayed`);
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification;
        console.log(`notifi5 : displayed`);
      });
      this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
        // Process your message as required
        console.log(`notifi6 : displayed`);
      });
    }
    
  }
  componentWillUnmount() {
    console.log(`comp will unMount Login`);
    if (this.unsubscriber) {
          //this.unsubscriber();
      }
  }
  clearStorage = async() => {
    try {
      await AsyncStorage.clear();
      this.setState({userUID:null, checked:false, isLoading:false});
    } 
    catch (error) {
      console.log(`error is here=${error.message}`);
    }
  }
  async checkUserDetails(userData, navigate) {
    try {
        console.log(`user details data sending...${userData}`);
        AsyncStorage.setItem('userUUID', JSON.stringify(userData.uid));
        var firebaseRefObj = firebase.database().ref('bottomsapp/users/' + userData.uid + '/userDetails');
        firebaseRefObj.once('value')
        .then(snapshot => {
          // snapshot.val() is the dictionary with all your keys/values from the '/store' path
          console.log(`user details=${JSON.stringify(snapshot.val())}`);
          if(snapshot.val()==null) {
            firebaseRefObj.set({
              email: userData.email,displayName: userData.displayName,
              photoURL: userData.photoURL,phoneNumber: userData.phoneNumber,
              isAdmin: false, userLocation: this.state.userLocation,
            }, function(error) {
              this.setState({isLoading:false, checked:true, userUID: userData.uid, loginEnabled: true});
              AsyncStorage.setItem('isAdmin', JSON.stringify(false));
              if (error) {
                  console.log(`got an error=${error}`);
              } else {
                  console.log(`user details saved on firebase`);
                  if(this.state.fromListing) {
                    this.props.navigation.goBack();
                    this.props.navigation.state.params.updateDataFromLogin(userData.uid,this.state.locationObj);
                  }
                  else {
                    navigate("ListingScreen", {fromLogin: true, userUID: userData.uid, 
                      locationObj: this.state.locationObj, updateDataFromListing: this.updateDataFromListing,});
                  } 
              }
            }.bind(this));
          }
          else {
            this.setState({isLoading:false, checked:true, userUID: userData.uid, loginEnabled: true});
            AsyncStorage.setItem('isAdmin', JSON.stringify(snapshot.val().isAdmin));
            if(this.state.fromListing) {
              this.props.navigation.goBack();
              this.props.navigation.state.params.updateDataFromLogin(userData.uid,this.state.locationObj);
            }
            else {
              navigate("ListingScreen", {fromLogin: true, userUID: userData.uid, 
                locationObj: this.state.locationObj, updateDataFromListing: this.updateDataFromListing,});
            } 
          }
        });
    } catch(error) {
        console.log(error);
    } 
  }
  onLoginFacebook = (navigate) => {
    console.log(`Facebook login pressed`);
    LoginManager
        .logInWithReadPermissions(['public_profile', 'email'])
        .then((result) => {
            if (result.isCancelled) {
                return Promise.reject(new Error('The user cancelled the request'));
            }
            this.setState({isLoading:true, checked:false, loginEnabled: false});
            console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
            // get the access token
            return AccessToken.getCurrentAccessToken();
        })
        .then(data => {
            const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
            return firebase.auth().signInWithCredential(credential);
        })
        .then((currentUser) => {
          console.log(`Facebook Login with user : ${JSON.stringify(currentUser.toJSON())}`);
          this.checkUserDetails(currentUser.toJSON(), navigate);
        })
        .catch((error) => {
          this.setState({isLoading:false, checked:true, loginEnabled: true});
          alert('Login failed. Please try again.');
          console.log(`Facebook login fail with error: ${error}`);
        });
  }
  onLoginGoogle = (navigate) => {
    console.log(`google login pressed`);
    GoogleSignin
        .signIn()
        .then((data) => {
            // create a new firebase credential with the token
            this.setState({isLoading:true, checked:false, loginEnabled: false});
            const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
            console.log(`Google Login with user credentails: ${credential}`);
            // login with credential
            return firebase.auth().signInWithCredential(credential);
        })
        .then((currentUser) => {
            console.log(`Google Login with user : ${JSON.stringify(currentUser.toJSON())}`);
            this.checkUserDetails(currentUser.toJSON(), navigate);          
        })
        .catch((error) => {
          this.setState({isLoading:false, checked:true, loginEnabled: true});
          alert('Login failed. Please try again.');
          console.log(`Login fail with error: ${error}`);
        });
  }
  
  changeLocation = async(locationName) => {
    
    console.log('location key='+locationName);
    let locations = this.state.locationsArray;
    let locObj = {};
    Object.keys(locations).map(function(key) {
      if(locations[key].name==locationName) {
        locObj = { id: key, name: locations[key].name };
      }
    });
    console.log('new locationObj='+JSON.stringify(locObj));        
    AsyncStorage.setItem('locationObj', JSON.stringify(locObj));
    this.setState({locationObj: locObj});
  }
  gotoListing = (navigate) => {
    console.log(`handing back press Login. ${JSON.stringify(this.state.fromListing)}`);
    if(this.state.fromListing) {
      this.props.navigation.goBack();
      this.props.navigation.state.params.updateDataFromLogin(this.state.userUID,this.state.locationObj);
    }
    else {
      navigate("ListingScreen", {fromLogin: true, userUID: this.state.userUID, 
        locationObj: this.state.locationObj, updateDataFromListing: this.updateDataFromListing,});
    }    
  }
  updateDataFromListing = (locationObj) => {
    console.log('coming from listing:'+JSON.stringify(locationObj));
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      if(isConnected) {
        this.fetchLocations(locationObj);            
      }
    });
  };
  fetchLocations = async(locationObj='') => {
    try {
      locationObj = this.state.locationObj ? this.state.locationObj : locationObj;
        var recentPostsRef = firebase.database().ref('bottomsapp/locations');
        console.log(`fetching location data url : ${recentPostsRef} location=${JSON.stringify(locationObj)}`);
        recentPostsRef.once('value').then(snapshot => {
            if(snapshot) {
                var tempLocations = snapshot.toJSON();
                var changeLocationObj = '';
                var isChanged=false;
                var locationArr = [];
                if(tempLocations) {
                  locationArr = new Array(Object.keys(tempLocations).length);
                }
                //console.log(`result locations= ${JSON.stringify(tempLocations)}`);
                Object.keys(tempLocations).map(function(key) {
                  console.log('key='+key+' data='+JSON.stringify(tempLocations[key]));
                  locationArr[tempLocations[key].index] = {'id': key, 
                  'name': tempLocations[key].name, 'hide': tempLocations[key].hide};
                  //console.log(' locObjArr='+JSON.stringify(locationArr));
                  if(!tempLocations[key].hide) {
                    changeLocationObj = {'id': key, 'name': tempLocations[key].name};
                  }
                  else if(locationObj&&tempLocations[key].name==locationObj.name) {
                    //if user selection location is now hidden
                    isChanged=true;
                  }
                });
                if(!(locationObj&&locationObj.id)) {
                  for(let i=0; i<locationArr.length;i++) {
                    //console.log(' locObjArr='+JSON.stringify(locationArr[i]));
                    if(!locationArr[i].hide) {
                      locationObj = {'id': locationArr[i].id, 'name': locationArr[i].name};
                      break;
                    }
                  }
                }
                if(isChanged) {
                  //location changed from hidden
                  locationObj=changeLocationObj;
                }
                console.log('isChange='+isChanged+' locObj='+JSON.stringify(locationObj));
                this.setState({locationsArray: tempLocations, locationObj: locationObj, 
                  isLoading: false });   
                (async () => {
                  AsyncStorage.setItem('locationObj', JSON.stringify(locationObj));
                  AsyncStorage.setItem('locationsArray', JSON.stringify(tempLocations));
                })();
            }             
        });
        NetInfo.isConnected.fetch().then(isConnected => {
          console.log('First, is ' + (isConnected ? 'online' : 'offline'));
          if(!isConnected) {
            (async () => {
              var locationsArray = await AsyncStorage.getItem('locationsArray');
              locationsArray = locationsArray&&JSON.parse(locationsArray) ? JSON.parse(locationsArray) : [];
              console.log(`location AsyncStorage fetching:${locationsArray}`);
              //to do set location object here in state
              this.setState({locationsArray: locationsArray, 
                isLoading: false, });   
            })();
                
          }
        });
    } catch(error) {
        console.log(error);
    }
  }
  onBack() {
    this.setState({checkingTerms: false});
    //this.refs['WEBVIEW_REF'].goBack();
  }
  render() {
    const { navigate } = this.props.navigation;
    console.log(`userUID1:${this.state.userUID} location=${JSON.stringify(this.state.locationObj)} 
    checked=${this.state.checked} locationArr=${JSON.stringify(this.state.locationsArray)}
    loading=${this.state.isLoading}`);
    var locations = this.state.locationsArray;
    var tempLocations = new Array(Object.keys(locations).length);
    var serviceItems = [];
    if(this.state.userUID) {
      Object.keys(locations).map(function(key) {
        if(!locations[key].hide) {
          let locObj = { id: key, name: locations[key].name };
          tempLocations[locations[key].index] = locObj;
        }
      });
      console.log('picker items='+JSON.stringify(tempLocations));
      serviceItems = tempLocations.map( (s, i) => {
          console.log('picker items: i='+i+' s='+JSON.stringify(s));
          return <Picker.Item key={i} value={s.name} label={s.name} />
      });
    }
    if(this.state.checkingTerms) {
      return (
        <View style={{flex: 1}}>
        <WebView
          ref={'WEBVIEW_REF'}
          source={{uri: 'https://docs.google.com/document/u/1/d/e/2PACX-1vRMKkBFsQFEoQwJKsof37h-HhYXwekExTCX7hQuBEQrVNkAFvQoZL8nw1tc_Qdul_fAT5XDc5ABWu52/pub'}}
          style={{marginTop: 20}}
        />
          <TouchableOpacity style={{height: 48, justifyContent: 'center', alignItems: 'center',}}
            disabled={!this.state.checkingTerms}
            onPress={this.onBack.bind(this)}>
            <Text allowFontScaling={false}>Go Back</Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <SafeAreaView style={styles.safeArea}>
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Image style={styles.appIcon} source={require('./images/opener.png')} />
          <Text allowFontScaling={false} style={styles.titleText}>BottomsApp</Text>
          
        </View>
        <View style={styles.termsContainer}>
          {this.state.isLoading&&
          <View style={styles.loading}>
                <ActivityIndicator size='large' color="#fff" />
            </View>}
          {this.state.userUID
          ? 
          <View style={{height: 100, flexDirection:'column', }}>
            {Platform.OS !== "android"
            ?
            <Picker
              selectedValue={this.state.locationObj.name}
              style={{ width:300, flex:1, justifyContent:'center'}}
              itemStyle={styles.pickerText}
              onValueChange={ (itemValue, itemIndex) => 
               this.changeLocation(itemValue)} >
              {serviceItems}
            </Picker>
            :
            <ModalSelector
              data={tempLocations}
              initValue={this.state.locationObj.name}
              optionTextStyle={{fontFamily:"RobotoSlab-Bold",fontSize: normalize(24),color:'#fff',}}
              sectionTextStyle={{fontFamily:"RobotoSlab-Bold",fontSize: normalize(24),color:'#fff',}}
              selectedItemTextStyle={{fontFamily:"RobotoSlab-Bold",fontSize: normalize(24),color:'#23395b',}}
              cancelTextStyle={{fontFamily:"RobotoSlab-Bold",fontSize: normalize(24),color:'#fff',}}
              selectTextStyle={{fontFamily:"RobotoSlab-Bold",fontSize: normalize(24),color:'#23395b',}}
              cancelText={<Icon name="times" size={28} color="#23395b" />}
              onChange={(option)=>{ this.changeLocation(option.label)}}>
              <TextInput allowFontScaling={false} 
                textAlign={'center'}
                style={{ height:48, borderWidth:1, borderColor:'#ccc', padding:10, width:300,
                justifyContent:'center',
                fontFamily:"RobotoSlab-Bold",fontSize: normalize(24),color:'#fff',}}
                value={this.state.locationObj.name} />
            </ModalSelector>
            }
          </View>
          :
          <View>{this.state.loginEnabled&&<View>
          <Text allowFontScaling={false} style={styles.termsText}>
          BottomsApp is for information purposes only.{"\n"}
          BottomsApp does not deliver drinks.{"\n"}
          Consumption of alcohol is injurious to health.
          {"\n"}{"\n"}
          </Text>
          <Text allowFontScaling={false} style={{fontFamily: "RobotoSlab-Bold", fontSize: normalize(14), textAlign: 'center',color: '#fff'}}
          onPress={() => this.setState({checkingTerms: true})}>
          Terms and Conditions</Text></View>}
          </View>
          }
          {this.state.loginEnabled&&!this.state.userUID&&<TouchableOpacity 
          style={{borderRadius: 5, marginTop: 20,}}
          onPress={() => this.setState({ checked: !this.state.checked })}>
            <Text allowFontScaling={false} style={{margin:4, fontSize: normalize(28), }}>
            {this.state.checked 
            ? <Icon_II name="md-checkbox" size={40} color="#fff" />
            : <Icon_II name="md-square" size={40} color="#fff" />
            }</Text>
          </TouchableOpacity>}
        </View>
        <View style={{flex:2}}>
        {this.state.loginEnabled&&
        <View style={{flex: 1, flexDirection:'row', justifyContent:'space-evenly' }}>
        <Button containerStyle={{ justifyContent: 'center',alignItems: 'center',
          }}
          style={{ fontFamily: "RobotoSlab-Bold", fontSize: normalize(18), color: 'white' }}
          onPress={() => 
            !this.state.userUID 
            ? this.state.checked&&this.onLoginFacebook(navigate)
            : this.clearStorage()
          }>
          {this.state.userUID
          ? <Icon name="power-off" size={40} color='#fff' />
          : <Icon name="facebook" size={48} color={this.state.checked ? '#4968AD' : '#fff'}  />
          }
        </Button>
        <Button containerStyle={{justifyContent: 'center',alignItems: 'center',
          }}
          style={{ fontFamily: "RobotoSlab-Bold", fontSize: normalize(18), color: 'white' }}
          onPress={() => 
            !this.state.userUID 
            ? this.state.checked&&this.onLoginGoogle(navigate)
            : this.gotoListing(navigate)
            }>
          {this.state.userUID
          ? <Icon_II name="md-arrow-round-forward" size={48} color='#fff'  />
          : <Icon name="google" size={48} color={this.state.checked ? '#CC5441' : '#fff'}  />
          }
        </Button>
        </View>
        }
        </View>
      </View>
      </SafeAreaView>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#14cda8',
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#14cda8'
  },
  titleContainer: {
    flex: 4,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  pickerText: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(24),
    color:'#fff',
  },
  termsContainer: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(28),
    color: '#fff',
    textAlign: 'center',
    margin: 10,
  },
  termsText: {
    fontFamily: "RobotoSlab-Bold",
    fontSize: normalize(14),
    color: '#fff',
    textAlign: 'center',
    marginTop: 20,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    height: 64,
    justifyContent: 'center',
    textAlign: 'center', 
    alignItems: 'center',
  },
  appIcon: {
    width: 52,
    height: 100,
    alignSelf: 'center',
  },
});
