/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { createStackNavigator } from 'react-navigation';
import {AsyncStorage } from 'react-native';
//import { packageInfo } from './package.json';
import Login from './Login';
import ItemsList from './ItemsList';
import Checkout from './Checkout';
import AddCategory from './AddCategory';
import AddItem from './AddItem';
import AddLocation from './AddLocation';
import StackViewStyleInterpolator from "react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator";
import { fromLeft } from 'react-navigation-transitions';

const VERSION = 'version';

const LoginStack = createStackNavigator({
  LoginScreen: { screen: Login},
    ListingScreen: { screen: ItemsList},
    AddItemScreen: { screen: AddItem},
    AddCategoryScreen: { screen: AddCategory},
    AddLocationScreen: {screen: AddLocation},
    CheckoutScreen: { screen: Checkout},
  }, { 
    headerMode: 'none' ,
    header: null,
  }
);
const RootStack = createStackNavigator({
    ListingScreen: { screen: ItemsList},
    LoginScreen: { screen: Login},
    AddItemScreen: { screen: AddItem},
    AddCategoryScreen: { screen: AddCategory},
    AddLocationScreen: {screen: AddLocation},
    CheckoutScreen: { screen: Checkout},
  }, { 
    headerMode: 'none' ,
    header: null,
    transitionConfig: (currentState) => {
      if (currentState.scenes[currentState.scenes.length - 1].route.routeName.includes("Login")) {
        return fromLeft()        
      }
      return {
        screenInterpolator: StackViewStyleInterpolator.forHorizontal,
      };
    },
  }
);

export default class App extends React.Component {
  state = {
    loading: false,
    userUID: null,
  };
  setAppLaunched = () => {
    AsyncStorage.multiSet([[VERSION, "1"],['userUUID', '']]);
  }
  async componentWillMount() {
    console.log(`comp will mount AppJS`);
    try {
      const hasLaunched = await AsyncStorage.getItem(VERSION);
      if (hasLaunched !== "1") {
        console.log('userUUID AppJS'+hasLaunched);
        this.setAppLaunched();
        this.setState({ loading: true });
        return true;
      }
      else {
        console.log(`userUUID2 AppJS:${hasLaunched}`);
        const value = await AsyncStorage.getItem('userUUID');
        this.setState({ userUID: JSON.parse(value), loading: true });
      }
    } catch (error) {
      // Error retrieving data
      console.log(`error is here=${error.message}`);
    }
  }
  render() {
    console.log(`render called userUUID=${this.state.userUID}`);
    if(this.state.loading ) {
      return this.state.userUID ? <RootStack /> : <LoginStack/> ;
    } else {
      return null;
    }
  }
}